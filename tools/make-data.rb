#! /usr/local/bin/ruby
## make-data.rb
## Login : <theo@richaro-rack>
## Started on  Tue Mar 29 23:21:17 2011 Theo Richart
## $Id$
##
## Author(s):
##  - Theo Richart <richar_o@epita.fr>
##

require 'csv'
require 'uri'
require 'zlib'
require 'open-uri'

require 'net/http'
require 'rubygems'
require 'nokogiri'
require "rexml/document"

class Writer
  def initialize(io)
    @io = io
  end

  def write(cat, list)
    return if list.length == 0
    @io.puts "#{cat}:"
    list.each do |x|
      x.write(@io)
    end
  end
end

class Author
  def initialize(name)
    @name = name
  end

  def name
    URI.escape(@name, Regexp.new("[^#{URI::PATTERN::UNRESERVED}]"))
  end

  def write(io)
    io.puts "  #{name}:"
    io.puts "    name: #{@name}"
  end
end

class Movie
  attr_accessor :hash, :cat
  def initialize(title, desc)
    @cat = "Piece"
    @hash  = {
      :title => title,
      :description => desc,
      :kind => 'Movie',
      :genre => 'bullshit'
    }
  end

  def name
    return URI.escape(@hash[:title], Regexp.new("[^#{URI::PATTERN::UNRESERVED}]"))
  end

  def write(io)
    io.puts "  #{name}:"
    @hash.each do |k, v|
      if k == :description then
        io.puts "    #{k}: |"
        v.split("\n").each { |line| io.puts "      #{line}" }
      else
        io.puts "    #{k}: \"#{v}\""
      end
    end
  end
end

class Helper
  def initialize
    @writer = Writer.new(STDOUT.to_io)
    @list = []
    @authors = []
  end

  def getDiscogs(params)
    url = "http://www.discogs.com/" + params +"?f=xml&api_key=02629a8d87"
    headers = {'Accept-Encoding' => 'gzip', 'User-Agent' => 'MyDiscogsClient/1.0 +http://mydiscogsclient.org'}

    begin
      response = open(url, headers)
      begin
        data = Zlib::GzipReader.new(response)
      rescue Zlib::GzipFile::Error
        response.seek(0)
        data = response.read
      end
    rescue OpenURI::HTTPError => e
      data = e.io.read
    end
    Nokogiri::XML(data)
  end

  def write
    @writer.write('Author', @authors)

    @writer.write('Piece', @list)
  end

  def parse(data)
    doc = REXML::Document.new data
    doc.elements.each('feed/movie') do |m|
      title = m.elements['title'].text
      desc = m.elements['synopsisShort'].text
      movie = Movie.new(title, desc)
      movie.hash[:genre] = m.elements['genreList'].elements[1].text
      movie.hash[:image] = m.elements['poster'].attributes['href']
      @list.push(movie)
    end
  end


  def genMovies(nb)
    url = "http://api.allocine.fr/xml/movielist?count=#{nb}&filter=nowshowing&format=xml&order=theatercount&page=1&partner=YW5kcm9pZC12Mg&version=2"
    data = Net::HTTP.get URI.parse(url)
    parse(data)
  end

  def parseRelease(id, album)
    begin
      release = getDiscogs("release/#{id}")
      album.hash[:genre] = release.xpath('resp/release/genres').first.text
      begin
        album.hash[:image] = release.xpath('resp/release/images/image').first['uri150']
        album.hash[:image] = release.xpath('resp/release/images/image').filter('[@type="primary"]').first['uri150']
        Thread.new { Net::HTTP.get URI.parse("http://www.discogs.com/release/" + id) }
      rescue
      end

      desc = ''
      release.xpath('resp/release/tracklist/track').each { |t|
        pos = t.at_xpath('position').text
        title = t.at_xpath('title').text
        desc += "#{pos} - #{title}\n" if pos != nil && !pos.empty? && title != nil && !title.empty?
      }
      album.hash[:description] = desc
    rescue Exception => e
      $stderr.puts e
    end
  end

  def cmp(a, b)
    return a.downcase.strip == b.downcase.strip
  end

  def parseDiscogs(nb, data)
    begin
      i = 1

      doc = Nokogiri::XML(data)
      threads = []
      doc.xpath('resp/label/releases/release').each do |rel|

        threads << Thread.new{
          title = rel.xpath('title').text.strip

          $stderr.puts "[#{i}/#{nb}] #{title}..."

          desc = rel.xpath('format').text.strip
          next if @list.index {|x| cmp(x.hash[:title], title) } != nil

          album = Movie.new("#{title}", desc)

          parseRelease(rel['id'], album)

          album.hash[:kind] = 'Music'

          author = Author.new(rel.xpath('artist').text.strip)
          album.hash[:Author] = author.name
          @authors.push author if @authors.index {|a| cmp(author.name, a.name)} == nil

          @list.push album
        }
        i = i + 1
        break if nb != -1 && i == nb
      end
    rescue Exception => e
      $stderr.puts e
    end
    threads.each { |t| t.join }
  end

  def genMusic(nb)
    f = File.open("atco.xml")
    parseDiscogs(nb, f)
    f.close
  end
end


kind = ARGV[0]

h = Helper.new
h.genMusic(100) if kind == 'all' || kind == 'music'
h.genMovies(80) if kind == 'all' || kind == 'movies'
h.write
