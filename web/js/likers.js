$(document).ready(function()
    {
        $('.likeLink').click(function(){
            var w = 300;
            var h = 300;

            $('.popup').remove();
            
            $('#content').append('<div class="popup">Loading...</div>');
            $('.popup').css('left', document.documentElement.clientWidth / 2 - w / 2);
            $('.popup').css('top', document.documentElement.clientHeight / 2 - h / 2);

            var url = $(this).attr('href');
            $('.popup').load(url, function(){
                $('.close').click(function(){
                    $('.popup').remove();
                    return false;
                });
            });

            return false;
        });
    });