function rate(nb) {
	for (i = 0; i < 6; i++) {
		$('.star' + i).children('img').attr(
				'src',
				'/images/star_' + (i == 0 ? 'z' : (i <= nb ? 'o' : 'n'))
						+ '.png').attr('title', i);
		if (i <= nb) {
			$('.star' + i)
			.animate(
			{
				"opacity" : "0.5"
			}, "fast")
			.animate( 
			{
				"opacity" : "1"
			}, "fast");
		}
	}
}

$(document).ready(
		function() {
			$('select').hide();

			var stars = '<ul class="stars">';
			for (i = 0; i < 6; i++) {
				stars += '<li class="star' + i
						+ ' star"><img src="/images/star_n.png" alt="' + i
						+ '"/></li>';
			}
			stars += '</ul>';

			$('select').parent().append(stars);

			rate($('select').val());

			for (i = 0; i < 6; i++) {
				$('.star' + i).click(function() {
					var mark = $(this).children('img').attr('alt');
					rate(mark);
					$('select').val(mark);
				});
			}

		});
