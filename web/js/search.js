/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function()
{
    $('#searchForm  input[type="submit"]').hide();

    $('#search').click(function() {
        this.value = '';
    });

    $('#search').val('Search...');

    $('#search').keyup(function(key)
    {
        if (this.value.length >= 3 || this.value == '')
        {
            var url = $(this).parents('form').attr('action') + '/ajax';
            $('#content').load(
                url, {
                    'ajax': true,
                    'search': this.value
                }
                );
        }
    });
});
