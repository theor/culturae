<table>
  <tbody>
    <tr>
      <th>Id:</th>
      <td><?php echo $author->getId() ?></td>
    </tr>
    <tr>
      <th>Name:</th>
      <td><?php echo $author->getName() ?></td>
    </tr>
    <tr>
      <th>Description:</th>
      <td><?php echo $author->getDescription() ?></td>
    </tr>
    <tr>
      <th>Image:</th>
      <td><?php echo $author->getImage() ?></td>
    </tr>
  </tbody>
</table>

<hr />

<a href="<?php echo url_for('author/edit?id='.$author->getId()) ?>">Edit</a>
&nbsp;
<a href="<?php echo url_for('author/index') ?>">List</a>
