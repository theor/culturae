<h1>Authors List</h1>

<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Name</th>
      <th>Description</th>
      <th>Image</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($authors as $author): ?>
    <tr>
      <td><a href="<?php echo url_for('author/show?id='.$author->getId()) ?>"><?php echo $author->getId() ?></a></td>
      <td><?php echo $author->getName() ?></td>
      <td><?php echo $author->getDescription() ?></td>
      <td><?php echo $author->getImage() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

  <a href="<?php echo url_for('author/new') ?>">New</a>
