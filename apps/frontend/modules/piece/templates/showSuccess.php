<?php use_javascript('likers.js') ?>
<img id="illustration" src="<?php echo $piece->getImage() ?>" alt="<?php echo $piece->getTitle() ?>" />

<?php for ($i = $grade; $i < 5; $i++): ?><img class="stars" src="/images/star_n.png" alt="0" /><?php endfor; ?>
<?php for ($i = 0; $i < $grade; $i++): ?><img class="stars" src="/images/star_o.png" alt="1" /><?php endfor; ?>


<a id="editButton" class="action_button" href="<?php echo url_for('piece/edit?id='.$piece->getId()) ?>">Edit piece</a>
<h1 class="bookTitle"><?php echo $piece->getTitle() ?></h1>
<h2><?php echo $piece->getKind() ?></h2>
<h3><?php echo $piece->getGenre() ?></h3>
<h4><?php echo $author ?></h4>
<hr />

<?php
	if (!$viewAllReviews)
	{
		$toDisplay = $piece->getDescription();
		$toDisplay = str_replace ("\n", "<br />", $toDisplay);
		$toDisplay = str_replace('&lt;b&gt;', '<b>', $toDisplay);
		$toDisplay = str_replace('&lt;/b&gt;', '</b>', $toDisplay);
		echo $toDisplay;
	}
?>

<div class="likeZone">
	<?php echo LinkHelper::getLikeLink('piece', $piece) ?>&nbsp;<?php echo LinkHelper::getDislikeLink('piece', $piece) ?>
	<br />
	<?php echo FeelingsHelper::GetLikersListDisplay($pieceLikers); ?>
</div>

<div id="reviews">
	
<?php
	$count = count($reviews);
	if ($count == 0): 
?>
		No review.
<?php
	else:
		$limit = 3;
		$count = $viewAllReviews ? $count : ($count > $limit ? $limit : $count);
		for ($j = 0; $j < $count; $j++): 
			$review = $reviews[$j];
			$reviewLikers = $reviewsLikers[$j];
	?>
			<div class="review">
				<img class="avatar" src="<?php echo $review->getUser()->getAvatarPath(); ?>" />
				
				<div class="likeZone">
					<?php echo LinkHelper::getLikeLink('review', $review) ?>&nbsp;<?php echo LinkHelper::getDislikeLink('review', $review) ?>
					<?php echo link_to('Comment', 'comment/new?review=' . $review->getId(), 'class=like_button') ?>
				</div>
				
<!--				<div class="content">-->
					<p class="name">
						<a name="review<?php echo $review->getId(); ?>" href="<?php echo url_for('profile/view?id=' . $review->getUser()->getId()); ?>"><?php echo $review->getUser()->getFullname(); ?></a>
					</p>
					
<?php for ($i = 0; $i < $review->getGrade(); $i++): ?><img class="star" src="/images/star_o.png" alt="1" /><?php endfor; ?>
<?php for ($i = $review->getGrade(); $i < 5; $i++): ?><img class="star" src="/images/star_n.png" alt="0" /><?php endfor; ?>
					
					<div class="message">"<?php echo $review->getDescription(); ?>"</div>
<!--				</div>-->
				
				<div class="newsDate">
					<?php echo FeelingsHelper::GetLikersListDisplay($reviewLikers); ?>
				</div>
		
				
			</div>
			
		<?php
			if ($viewAllReviews):
				for($c = 0; $c < count($comments[$j]); $c++):
					$comment = $comments[$j][$c];
					$author = $comment->getUser();
		?>			
					<div class="comment">
						<img class="avatar" src="<?php echo $author->getAvatarPath(); ?>" />
						
						<div class="likeZone">
							<?php echo LinkHelper::getLikeLink('comment', $comment) ?>&nbsp;<?php echo LinkHelper::getDislikeLink('comment', $comment) ?>
						</div>
						
						<p class="name">
							<a name="comment<?php echo $comment->getId(); ?>" href="<?php echo url_for('profile/view?id=' . $author->getId()); ?>"><?php echo $author->getFullname(); ?></a>
						</p>
						
						<div class="message">
							"<?php echo $comment->getText(); ?>"
						</div>
						
						<div class="newsDate">
							<?php echo FeelingsHelper::GetLikersListDisplay($commentsLikers[$j][$c]); ?>
						</div>
												
					</div>
		<?php
				endfor;		
			endif;
		?>
	<?php endfor; ?>
<?php endif; ?>

	</div>
		<?php if (!$viewAllReviews): ?>
			<a class="action_button" href="<?php echo url_for('piece/show?id=' . $piece->getId() . '&viewAllReviews=1'); ?>">View all reviews</a>
		<?php else: ?>
			<a class="action_button" href="<?php echo url_for('piece/show?id=' . $piece->getId()); ?>">Back</a>
		<?php endif; ?>
		
        <?php
            if($myreview == null)
                echo link_to('Write a review', 'review/new?piece=' . $piece->getId(), array('class' => 'action_button'));
            else
                echo link_to('Edit My Review', 'review/edit?id='. $myreview->getId() . '&piece='.$piece->getId(), array('class' => 'action_button'));
?>
