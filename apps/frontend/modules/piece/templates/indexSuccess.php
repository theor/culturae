<h1>Pieces List</h1>

<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Title</th>
      <th>Description</th>
      <th>Image</th>
      <th>Kind</th>
      <th>Genre</th>
      <th>Author</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($pieces as $piece): ?>
    <tr>
      <td><a href="<?php echo url_for('piece/show?id='.$piece->getId()) ?>"><?php echo $piece->getId() ?></a></td>
      <td><?php echo $piece->getTitle() ?></td>
      <td><?php echo $piece->getDescription() ?></td>
      <td><?php echo $piece->getImage() ?></td>
      <td><?php echo $piece->getKind() ?></td>
      <td><?php echo $piece->getGenre() ?></td>
      <td><?php echo $piece->getAuthorId() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

  <a href="<?php echo url_for('piece/new') ?>">New</a>
