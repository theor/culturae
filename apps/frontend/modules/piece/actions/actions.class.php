<?php

/**
 * piece actions.
 *
 * @package    culturae
 * @subpackage piece
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class pieceActions extends sfActions
{
    public function executeIndex(sfWebRequest $request)
    {
        $this->pieces = Doctrine_Core::getTable('Piece')
                        ->createQuery('a')
                        ->execute();
    }

    public function executeShow(sfWebRequest $request)
    {
        $this->piece = Doctrine_Core::getTable('Piece')->find(array($request->getParameter('id')));
        $this->pieceLikers = FeelingsHelper::GetLikersDisplay($this->piece, $this->getController(), 'piece');
        
        $this->getResponse()->setTitle('Culturae: ' . $this->piece->getTitle(), false);
        
        $this->forward404Unless($this->piece);
        $this->comments = array();
        $this->reviewsLikers = array();
        $this->commentsLikers = array();
        $this->viewAllReviews = $request->getParameter('viewAllReviews', false);
        $this->reviews = Doctrine_Query::create()
                        ->from('Review r')
                        ->where('r.piece_id = ' . $this->piece->getId())
                        ->orderBy('id DESC')
                        ->execute();
        $this->grade = 0;
        $size = count($this->reviews);
        for ($k = 0; $k < $size; $k++)
        {
            $review = $this->reviews[$k];
            $this->grade += $review->getGrade();
            $this->comments[$k] = Doctrine_Core::getTable("Comment")->findByReviewId($review->getId());            
            $this->reviewsLikers[$k] = FeelingsHelper::GetLikersDisplay($review, $this->getController(), 'review');
            
            $this->commentsLikers[$k] = array();
            for($l = 0; $l < count($this->comments[$k]); $l++)
            {
            	$this->commentsLikers[$k][$l] = FeelingsHelper::GetLikersDisplay($this->comments[$k][$l], $this->getController(), 'comment');
            }
        }
        $this->grade /= ( $size > 0 ? $size : 1);
        $this->grade = round($this->grade);
        
        $count = count($this->reviews);
        $test = $this->piece->getAuthorId();
        $this->author = isset($test) ? $this->piece->getAuthor()->getName() : "";

        $this->myreview = Doctrine_Core::getTable('Review')->findOneByUserIdAndPieceId(Auth::getUserLogged()->getId(), $this->piece->getId());
    }

    public function executeNew(sfWebRequest $request)
    {
    	$this->getResponse()->setTitle('Culturae - New piece', false);
    	
        $this->form = new PieceForm();
    }

    public function executeCreate(sfWebRequest $request)
    {
        $this->forward404Unless($request->isMethod(sfRequest::POST));

        $this->form = new PieceForm();

        $this->processForm($request, $this->form);

        $this->setTemplate('new');
    }

    public function executeEdit(sfWebRequest $request)
    {
    	$this->getResponse()->setTitle('Culturae - Edit piece', false);
    	
        $this->forward404Unless($piece = Doctrine_Core::getTable('Piece')->find(array($request->getParameter('id'))), sprintf('Object piece does not exist (%s).', $request->getParameter('id')));
        $this->form = new PieceForm($piece);
    }

    public function executeUpdate(sfWebRequest $request)
    {
        $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
        $this->forward404Unless($piece = Doctrine_Core::getTable('Piece')->find(array($request->getParameter('id'))), sprintf('Object piece does not exist (%s).', $request->getParameter('id')));
        $this->form = new PieceForm($piece);

        $this->processForm($request, $this->form);

        $this->setTemplate('edit');
    }

    public function executeDelete(sfWebRequest $request)
    {
        $request->checkCSRFProtection();

        $this->forward404Unless($piece = Doctrine_Core::getTable('Piece')->find(array($request->getParameter('id'))), sprintf('Object piece does not exist (%s).', $request->getParameter('id')));
        $piece->delete();

        $this->redirect('piece/index');
    }

    public function executeLikers(sfWebRequest $request)
    {
        $this->likers = Doctrine_Core::getTable('Piece')->findOneById($request->getParameter('id'))->getLikers();
    }

    public function executeLike(sfWebRequest $request)
    {
        $this->likeDislike(true, $request);
    }

    public function executeDislike(sfWebRequest $request)
    {
        $this->likeDislike(false, $request);
    }

    private function likeDislike($isLike, sfWebRequest $request)
    {
        $id = $request->getParameter('id');
        $this->piece = Doctrine_Core::getTable('Piece')->findOneById($id);
        $this->forward404Unless($this->piece);

        $like = Doctrine_Core::getTable('Feeling')->findOneByUserIdAndPieceId(Auth::getUserLogged()->getId(), $this->piece->getId());

        if ($like == null) {
            $like = new Feeling();
            $like->setPiece($this->piece);
            $like->setUser(Auth::getUserLogged());
        } else {
            if ($like->getValue() == ($isLike ? 0 : 1)) {
                $like->delete();
                return $this->redirect('piece/show?id=' . $id);
            }
        }

        $like->setValue($isLike ? 0 : 1);
        $like->save();
        $this->redirect('piece/show?id=' . $id);
    }

    protected function processForm(sfWebRequest $request, sfForm $form)
    {
        $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
        if ($form->isValid()) {
            $piece = $form->save();

            $this->redirect('piece/edit?id=' . $piece->getId());
        }
    }
}
