<?php

/**
 * comment actions.
 *
 * @package    culturae
 * @subpackage comment
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class commentActions extends sfActions
{
    public function executeIndex(sfWebRequest $request)
    {
        $this->comments = Doctrine_Core::getTable('Comment')
                        ->createQuery('a')
                        ->execute();
    }

    public function executeShow(sfWebRequest $request)
    {
        $this->comment = Doctrine_Core::getTable('Comment')->find(array($request->getParameter('id')));
        $this->forward404Unless($this->comment);
    }

    public function executeNew(sfWebRequest $request)
    {
    	$this->getResponse()->setTitle('Culturae - New comment', false);
    	
        $id = $request->getParameter('review', null);
        $this->forward404If($id == null);
        $this->form = new CommentForm(null, array('review' => $id));
        $this->review = Doctrine_Core::getTable('Review')->findOneById($id);
    }

    public function executeCreate(sfWebRequest $request)
    {
        $this->forward404Unless($request->isMethod(sfRequest::POST));

        $this->form = new CommentForm();

        $this->processForm($request, $this->form);

        $this->setTemplate('new');
    }

    public function executeEdit(sfWebRequest $request)
    {
        $this->forward404Unless($comment = Doctrine_Core::getTable('Comment')->find(array($request->getParameter('id'))), sprintf('Object comment does not exist (%s).', $request->getParameter('id')));
        $this->form = new CommentForm($comment);
    }

    public function executeUpdate(sfWebRequest $request)
    {
        $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
        $this->forward404Unless($comment = Doctrine_Core::getTable('Comment')->find(array($request->getParameter('id'))), sprintf('Object comment does not exist (%s).', $request->getParameter('id')));
        $this->form = new CommentForm($comment);
        foreach ($comments[$j] as $comment)
            $author = $comment->getUser();
        $this->processForm($request, $this->form);

        $this->setTemplate('edit');
    }

    public function executeDelete(sfWebRequest $request)
    {
        $request->checkCSRFProtection();

        $this->forward404Unless($comment = Doctrine_Core::getTable('Comment')->find(array($request->getParameter('id'))), sprintf('Object comment does not exist (%s).', $request->getParameter('id')));
        $comment->delete();

        $this->redirect('comment/index');
    }

    public function executeLikers(sfWebRequest $request)
    {
        $this->likers = Doctrine_Core::getTable('Comment')->findOneById($request->getParameter('id'))->getLikers();
    }

    public function executeLike(sfWebRequest $request)
    {
        $this->likeDislike(true, $request);
    }

    public function executeDislike(sfWebRequest $request)
    {
        $this->likeDislike(false, $request);
    }

    private function likeDislike($isLike, sfWebRequest $request)
    {
        $id = $request->getParameter('id');
        $this->comment = Doctrine_Core::getTable('Comment')->findOneById($id);
        $this->forward404Unless($this->comment);

        $like = Doctrine_Core::getTable('Feeling')->findOneByUserIdAndCommentId(Auth::getUserLogged()->getId(), $this->comment->getId());

        if ($like == null) {
            $like = new Feeling();
            $like->setComment($this->comment);
            $like->setUser(Auth::getUserLogged());
        } else {
            if ($like->getValue() == ($isLike ? 0 : 1)) {
                $like->delete();
                return $this->redirect('piece/show?id=' . $this->comment->getReview()->getPieceId() . '&viewAllReviews=1#comment' . $this->comment->getId());
            }
        }

        $like->setValue($isLike ? 0 : 1);
        $like->save();
        $this->redirect('piece/show?id=' . $this->comment->getReview()->getPieceId() . '&viewAllReviews=1#comment' . $this->comment->getId());
    }

    protected function processForm(sfWebRequest $request, sfForm $form)
    {
        $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
        if ($form->isValid()) {
            $comment = $form->save();

            return $this->redirect('piece/show?id=' . $comment->getReview()->getPieceId() . '&viewAllReviews=1#review' . $comment->getReviewId());
        }
        // die("INVALID");
    }

}
