<table>
  <tbody>
    <tr>
      <th>Id:</th>
      <td><?php echo $comment->getId() ?></td>
    </tr>
    <tr>
      <th>Text:</th>
      <td><?php echo $comment->getText() ?></td>
    </tr>
    <tr>
      <th>User:</th>
      <td><?php echo $comment->getUserId() ?></td>
    </tr>
    <tr>
      <th>Review:</th>
      <td><?php echo $comment->getReviewId() ?></td>
    </tr>
  </tbody>
</table>

<hr />

<a href="<?php echo url_for('comment/edit?id='.$comment->getId()) ?>">Edit</a>
&nbsp;
<a href="<?php echo url_for('comment/index') ?>">List</a>
