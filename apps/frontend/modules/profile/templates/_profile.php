	<div class="menu_bg profile_summary">
	<img class="avatar" src="<? echo Auth::getUserLogged()->getAvatarPath(); ?>" alt="Avatar" />
    <?php echo "<h2>" . $name . "</h2>"; ?>
   
    <span class="mail"><?php echo $mail; ?></span>
    <a id="logout" class="like_button" href="<?php echo url_for('home/logout'); ?>">Logout</a>
   </div> 
    
    <?php if (count($likes) > 0): ?>
    <h3 class="likes">Likes</h3>
    <div class="menu_bg">
    	<ul>
    		<?php foreach($likes as $like): ?>
	    		<li>
	    			<a href="<?php echo url_for("piece/show?id=" . $like->getPiece()->getId()); ?>"><?php echo $like->getPiece()->getTitle(); ?></a>
	    		</li>
	    	<?php endforeach; ?>
    	</ul>
    </div>
    <?php endif; ?>
    
    <?php if (count($suggestions) > 0): ?>
    <h3 class="suggestions">Suggestions</h3>
    <div class="menu_bg">
    	<ul>
    		<?php foreach($suggestions as $suggestion): ?>
	    		<li>
	    			<a href="<?php echo url_for("piece/show?id=" . $suggestion->getPiece()->getId()); ?>"><?php echo $suggestion->getPiece()->getTitle(); ?> <?php echo ' (' . $suggestion->getUser()->getFullname() . ')'; ?></a>
	    		</li>
	    	<?php endforeach; ?>
    	</ul>
    </div>
    <?php endif; ?>
    
    <?php if (count($prescribers) > 0): ?>
    <h3 class="prescribers">Prescribers</h3>
    <div class="menu_bg">
    	<ul>
    		<?php foreach($prescribers as $prescriber): ?>
    			<li><a href="<?php echo url_for("profile/view?id=" . $prescriber->getId()); ?>"><?php echo $prescriber->getFullname(); ?></a></li>
    		<?php endforeach; ?>
    	</ul>
    </div>
    <?php endif; ?>