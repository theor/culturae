<div id="profilepage">
<div id="profile">
	<img class="avatar" src="<? echo $user->getAvatarPath(); ?>" alt="Avatar" />
	<?php echo "<h2>" . $user->getFirstname() . " " . $user->getLastname() . "</h2>"; ?>
	    
	<span class="mail"><?php echo $user->getMail(); ?></span>
	
	<?php if (Auth::getUserLogged()->getId() != $user->getId()): ?>
		<?php if (ProfileHelper::canSubscribe($user->getId())): ?>
			<a href="<?php echo url_for('profile/subscribe?id=' . $user->getId()) ?>">Subscribe</a>
		<?php endif; ?>
		<?php if (ProfileHelper::canUnsubscribe($user->getId())): ?>
			<a href="<?php echo url_for('profile/unsubscribe?id=' . $user->getId()) ?>">Unsubscribe</a>
		<?php endif; ?>
		
		<a href="<?php echo url_for('message/new?addressee_id=' . $user->getId()); ?>">Send message</a>
	<?php endif; ?>
</div>

<div id="prescribers">
	<h3>Prescribers</h3>
	<div class="content_bg">
		<?php if (count($prescribers) > 0): ?>
		<a class="like_button more" href="<?php echo url_for("profile/view?id=" . $user->getId() . "&more=prescribers"); ?>">More</a>
		<ul>
			<?php foreach($prescribers as $prescriber): ?>
	    		<li>
	    			<img src="<?php echo $prescriber->getAvatarPath(); ?>" width="30" height="20" alt="<?php echo $prescriber->getFullname(); ?>" />
	    			<a href="<?php echo url_for("profile/view?id=" . $prescriber->getId()); ?>"><?php echo $prescriber->getFullname(); ?></a>
	    		</li>
	    	<?php endforeach; ?>
		</ul>
		<?php else: ?>
			Nobody
		<?php endif; ?>
		
	</div>
</div>

<div id="suggestions">
	<h3>Suggestions</h3>
	<div class="content_bg">
		<?php if (count($suggestions) > 0): ?>
		<a class="like_button more" href="<?php echo url_for("profile/view?id=" . $user->getId() . "&more=suggestions"); ?>">More</a>
		<ul>
			<?php foreach($suggestions as $suggestion): ?>
	    		<li>
	    			<img src="<?php echo $suggestion->getPiece()->getImage(); ?>" alt="<?php echo $suggestion->getPiece()->getTitle(); ?>" />
	    			<a href="<?php echo url_for("piece/show?id=" . $suggestion->getPiece()->getId()); ?>"><?php echo $suggestion->getPiece()->getTitle(); ?></a>
	    		</li>
	    	<?php endforeach; ?>
		</ul>
		<?php else: ?>
			Nothing
		<?php endif; ?>
	</div>
</div>

<div id="feelings">
	<h3>Likes</h3>
	<div class="content_bg">
		<?php if (count($likes) > 0): ?>
		<a class="like_button more" href="<?php echo url_for("profile/view?id=" . $user->getId() . "&more=likes"); ?>">More</a>
		<ul>
			<?php foreach($likes as $like): ?>
	    		<li>
	    			<img src="<?php echo $like->getPiece()->getImage(); ?>" alt="<?php echo $like->getPiece()->getTitle(); ?>" />
	    			<a href="<?php echo url_for("piece/show?id=" . $like->getPiece()->getId()); ?>"><?php echo $like->getPiece()->getTitle(); ?></a>
	    		</li>
	    	<?php endforeach; ?>
		</ul>
		<?php else: ?>
			Nothing
		<?php endif; ?>
	</div>
	
	<h3>Dislikes</h3>
	<div class="content_bg">
		<?php if (count($dislikes) > 0): ?>
		<a class="like_button more" href="<?php echo url_for("profile/view?id=" . $user->getId() . "&more=dislikes"); ?>">More</a>
		<ul>
			<?php foreach($dislikes as $dislike): ?>
	    		<li>
	    			<img src="<?php echo $dislike->getPiece()->getImage(); ?>" alt="<?php echo $dislike->getPiece()->getTitle(); ?>" />
	    			<a href="<?php echo url_for("piece/show?id=" . $dislike->getPiece()->getId()); ?>"><?php echo $dislike->getPiece()->getTitle(); ?></a>
	    		</li>
	    	<?php endforeach; ?>
		</ul>
		<?php else: ?>
			Nothing
		<?php endif; ?>
	</div>
</div>

<div id="profileContent">
	<?php  if ($more != ""): ?>
		<div class="content_bg">
		<?php if ($more == "likes"): ?>
			<ul>
			<?php foreach ($more_likes as $more_like): ?>
				<li>
	    			<img src="<?php echo $more_like->getPiece()->getImage(); ?>" alt="<?php echo $more_like->getPiece()->getTitle(); ?>" />
	    			<a href="<?php echo url_for("piece/show?id=" . $more_like->getPiece()->getId()); ?>"><?php echo $more_like->getPiece()->getTitle(); ?></a>
	    		</li>
			<?php endforeach; ?>
			</ul>
		<?php elseif ($more == "dislikes"): ?>
			<ul>
			<?php foreach ($more_dislikes as $more_dislike): ?>
				<li>
	    			<img src="<?php echo $more_dislike->getPiece()->getImage(); ?>" alt="<?php echo $more_dislike->getPiece()->getTitle(); ?>" />
	    			<a href="<?php echo url_for("piece/show?id=" . $more_dislike->getPiece()->getId()); ?>"><?php echo $more_dislike->getPiece()->getTitle(); ?></a>
	    		</li>
			<?php endforeach; ?>
			</ul>
		<?php elseif ($more == "prescribers"): ?>
			<ul>
			<?php foreach($more_prescribers as $more_prescriber): ?>
	    		<li>
	    			<img src="<?php echo $more_prescriber->getAvatarPath(); ?>" width="30" height="20" alt="<?php echo $more_prescriber->getFullname(); ?>" />
	    			<a href="<?php echo url_for("profile/view?id=" . $more_prescriber->getId()); ?>"><?php echo $more_prescriber->getFullname(); ?></a>
	    		</li>
	    	<?php endforeach; ?>
			</ul>
		<?php elseif ($more == "suggestions"): ?>
			<ul>
			<?php foreach($more_suggestions as $more_suggestion): ?>
	    		<li>
	    			<img src="<?php echo $more_suggestion->getPiece()->getImage(); ?>" alt="<?php echo $more_suggestion->getPiece()->getTitle(); ?>" />
	    			<a href="<?php echo url_for("piece/show?id=" . $more_suggestion->getPiece()->getId()); ?>"><?php echo $more_suggestion->getPiece()->getTitle(); ?></a>
	    		</li>
	    	<?php endforeach; ?>
			</ul>                
		<?php endif; ?>		
	<?php else: ?>
		<div>
			<ul>
            <?php foreach ($feed as $x) : ?>
            	<li>
            		<?php echo feedHelper::format($x) ?>
            	</li>
            <?php endforeach; ?>
           	</ul>
	<?php endif; ?>
	</div>
</div>
</div>