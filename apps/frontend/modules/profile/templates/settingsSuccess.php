<h1>Settings</h1>

<div id="settings_form">
	<form action="<?php echo url_for('profile/settings') ?>" method="POST" enctype="multipart/form-data">
	  <table>
	    <?php echo $form ?>
	    <tr>
	      <td colspan="2" align="center">
	        <input class="submitButton" type="submit" value="Save changes" />
	      </td>
	    </tr>
	  </table>
	</form>
	<?php if ($sf_user->hasFlash('error_settings')): ?>
	  <div class="flash_notice"><?php echo $sf_user->getFlash('error_settings') ?></div>
	<?php endif ?>	
</div>
<div>
    <?php if($ai != null): ?>
    <h1>Connected Account</h1>
    <img width="100" alt="<?php echo settingsHelper::getSiteName($ai); ?>" src="<?php echo settingsHelper::getSiteLogo($ai) ?>"/>
    <?php echo settingsHelper::getSiteName($ai); ?>
    <br />
    <?php echo settingsHelper::getAccountName($ai); ?>
    <?php endif; ?>
</div>