<?php

class profileComponents extends sfComponents
{
  public function executeProfile()
  {
  	$user = Auth::getUserLogged();
  	if ($user == null)
  	{
  		return sfView::ERROR;
  	}
  	$this->name = $user->getFullname();
  	$this->mail = $user->getMail();
  	$this->avatar = $user->getAvatar();

    $this->unreadMessages = Doctrine_Core::getTable('Message')->findByAddresseeIdAndIsnew($user->getId(), 1)->count();
  	 	
 	$this->likes = ProfileHelper::getLikes($user, 5);
  	$this->prescribers = ProfileHelper::getPrescribers($user, 5);	
  	$this->suggestions = ProfileHelper::getSuggestions($user, 5);
  }
}

?>