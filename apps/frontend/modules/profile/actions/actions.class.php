<?php

class profileActions extends sfActions
{
    public function executeView(sfWebRequest $request)
    {
        $id = $request->getParameter("id", Auth::getUserLogged()->getId());
        $this->user = Doctrine_Core::getTable("User")->FindOneById($id);

        $this->getResponse()->setTitle('Culturae - Profile of ' . $this->user->getFullname(), false);

        $this->likes = ProfileHelper::getLikes($this->user, 3);
        $this->dislikes = ProfileHelper::getDislikes($this->user, 3);
        $this->prescribers = ProfileHelper::getPrescribers($this->user, 3);
        $this->suggestions = ProfileHelper::getSuggestions($this->user, 3);

        $this->more = $request->getParameter("more", "");
        if ($this->more == "likes")
        {
            $this->more_likes = ProfileHelper::getLikes($this->user);
        }
        else if ($this->more == "dislikes")
        {
            $this->more_dislikes = ProfileHelper::getDislikes($this->user);
        }
        else if ($this->more == "prescribers")
        {
            $this->more_prescribers = ProfileHelper::getPrescribers($this->user);
        }
        else if ($this->more == "suggestions")
        {
            $this->more_suggestions = ProfileHelper::getSuggestions($this->user);
        }
        else
        {
            $this->feed = $this->getFeed($id);
        }
    }

    public function executeSettings(sfWebRequest $request)
    {
        $this->getResponse()->setTitle('Culturae - Settings', false);

        $me = Auth::getUserLogged();

        $this->ai = sfContext::getInstance()->getUser()->getAttribute("auth_info", null);

        $this->form = new SettingsForm(
                        array('firstname' => $me->getFirstname(),
                            'lastname' => $me->getLastname(),
                            'mail' => $me->getMail(),
                            'avatar' => $me->getAvatar()));

        if ($request->isMethod('post'))
        {
            $this->form->bind($request->getParameter('settings'), $request->getFiles('settings'));
            if ($this->form->isValid())
            {
                return $this->saveSettings($this->form->getValues());
            }
        }
    }

    public function executeSubscribe(sfWebRequest $request)
    {
        $id = $request->getParameter('id');

        $pr = new PrescriptorReference();
        $pr->setFollowerId(Auth::getUserLogged()->getId());
        $pr->setPrescriptorId($id);
        $pr->save();

        return $this->redirect('profile/view?id=' . $id);
    }

    public function executeUnsubscribe(sfWebRequest $request)
    {
        $id = $request->getParameter('id');

        $pr = Doctrine_Core::getTable('PrescriptorReference')->findOneByPrescriptorIdAndFollowerId($id, Auth::getUserLogged()->getId());
        $pr->delete();

        return $this->redirect('profile/view?id=' . $id);
    }

    public function saveSettings($values)
    {
//		$addressee = $values["addressee"];
//        $text = $values["text"];
//
//        if ($addressee == "" || $text == "") {
//            $this->getUser()->setFlash('error_message', 'You must fill all fields !');
//
//            $url = $this->generateUrl("default", array("module" => "profile", "action" => "settings"));
//            return $this->redirect($url);
//        }

        $firstname = $values['firstname'];
        $lastname = $values['lastname'];
        $login = $values['mail'];
        $password = $values['password'];
        $password_confirm = $values['confirmPassword'];
        $avatar = $values['avatar'];

        if ($password_confirm != "" && $password != $password_confirm)
        {
            $this->getUser()->setFlash('error_settings', 'Please re-type your password correctly !');

            $url = $this->generateUrl("default", array("module" => "profile", "action" => "settings"));
            return $this->redirect($url);
        }

        $me = Auth::getUserLogged();
        $checkUser = Doctrine_Core::getTable("User")->findOneByMail($login);
        if ($checkUser != NULL && $checkUser->getId() != $me->getId())
        {
            $this->getUser()->setFlash('error_settings', 'This mail is alredy used by another user !');

            $url = $this->generateUrl("default", array("module" => "profile", "action" => "settings"));
            return $this->redirect($url);
        }

        $me->setMail($login);
        $me->setFirstname($firstname);
        $me->setLastname($lastname);
        if ($password != "" && $password_confirm != "")
        {
            $me->setPass($password);
        }
        if ($avatar != NULL)
        {
            $filename = 'user_' . sha1($avatar->getOriginalName());
            $extension = $avatar->getExtension($avatar->getOriginalExtension());

            $file_dst = $filename . $extension;
            $avatar->save(sfConfig::get('sf_upload_dir') . '/' . $file_dst);
            $me->setAvatar($file_dst);
        }
        $me->save();

        $url = $this->generateUrl("default", array("module" => "profile", "action" => "settings"));
        return $this->redirect($url);
    }

    public function getFeed($id)
    {
        $feed = array();
        $likes = Doctrine_Query::create()
                        ->select('*')
                        ->from('Feeling f')
                        ->where('f.user_id = ?', $id)
                        ->orderBy('f.updated_at DESC')
                        ->execute();
        foreach ($likes as $like)
            array_push($feed, new News(News::FEELING, $like, $like->getUpdatedAt()));

        $reviews = Doctrine_Query::create()
                        ->from('Review r')
                        ->where('r.user_id = ?', $id)
                        ->orderBy('r.created_at DESC')
                        ->execute();
        foreach ($reviews as $rev)
            array_push($feed, new News(News::REVIEW, $rev, $rev->getCreatedAt(), FeelingsHelper::GetLikersDisplay($rev, $this->getController(), 'review')));

        $comments = Doctrine_Query::create()
                        ->from('Comment f')
                        ->where('f.user_id = ?', $id)
                        ->orderBy('f.created_at DESC')
                        ->execute();

        foreach ($comments as $com)
            array_push($feed, new News(News::COMMENT, $com, $com->getCreatedAt(), FeelingsHelper::GetLikersDisplay($com, $this->getController(), 'comment')));

        usort($feed, "feedHelper::cmp_news");

        return $feed;
    }

}