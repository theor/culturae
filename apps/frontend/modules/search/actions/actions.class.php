<?php

/**
 * search actions.
 *
 * @package    culturae
 * @subpackage search
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class searchActions extends sfActions
{

    private function search(sfWebRequest $request)
    {
    	$this->getResponse()->setTitle('Culturae - Search results', false);
    	
        $this->url = ($request->getRelativeUrlRoot());

        $this->search = $request->getParameter('search', '');
        $this->kind = $request->getParameter('kind', 'piece');

        $this->urlAuthors = $this->generateUrl("default", array("module" => "search",
                    "action" => "index", "kind" => "author",
                    "search" => $this->search));
        $this->urlUsers = $this->generateUrl("default", array("module" => "search",
                    "action" => "index", "kind" => "user",
                    "search" => $this->search));
        $this->urlPieces = $this->generateUrl("default", array("module" => "search",
                    "action" => "index", "kind" => "piece",
                    "search" => $this->search));

        switch ($this->kind) {
            case 'author':
                $this->format = 'formatAuthor';
                $query = Doctrine_Query::create()
                                ->from('Author p')
                                ->where("p.name like '%" . $this->search . "%'");
                break;
            case 'user':
                $this->format = 'formatUser';
                $query = Doctrine_Query::create()
                                ->from('User p')
                                ->where("p.firstname like '%" . $this->search . "%'")
                                ->orWhere("p.lastname like '%" . $this->search . "%'");
                break;

            default: // 'piece'
                $this->format = 'formatPiece';
                $query = Doctrine_Query::create()
                                ->from('Piece p')
                                ->where("p.Title like '%" . $this->search . "%'");
                break;
        }

        $this->results = new sfDoctrinePager($this->kind, 5);
        $this->results->setQuery($query);
        $this->results->setPage($request->getParameter('page', 1));
        $this->results->init();
    }

    /**
     * Executes index action
     *
     * @param sfRequest $request A request object
     */
    public function executeIndex(sfWebRequest $request)
    {
        if ($request->isMethod('POST')) {
            $url = $this->generateUrl("default", array("module" => "search",
                        "action" => "index", "kind" => "piece",
                        "search" => $request->getPostParameter('search', '')));
            return $this->redirect($url);
        }

        $this->search($request);
    }

    public function executeAjax(sfWebRequest $request)
    {
            $this->setLayout(false);
            $this->search($request);
            $this->setTemplate('index');
    }
}
