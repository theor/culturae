<div id="searchContent">
	<h1>Search results for "<?php echo $search ?>"</h1>
	
	Category filter:
	<ul id="categoryFilter">
	    <li><?php echo link_to_if($kind != 'piece', 'Pieces', $urlPieces) ?></li>
	    <li><?php echo link_to_if($kind != 'user', 'Users', $urlUsers) ?></li>
	    <li><?php echo link_to_if($kind != 'author', 'Authors', $urlAuthors) ?></li>
	</ul>
	
	<div class="pagination_desc">
	  <strong><?php echo count($results) ?></strong> result(s) found.
	</div>
	
	<?php if ($results->haveToPaginate()): ?>
	  <div class="pagination">
	    <a href="<?php echo $url ?>?page=1">&laquo;</a>
	    <a href="<?php echo $url ?>?page=<?php echo $results->getPreviousPage() ?>">&lsaquo;</a>
	
	    <?php foreach ($results->getLinks() as $page): ?>
	      <?php if ($page == $results->getPage()): ?>
	        <?php echo $page ?>
	      <?php else: ?>
	        <a href="<?php echo $url ?>?page=<?php echo $page ?>"><?php echo $page ?></a>
	      <?php endif; ?>
	    <?php endforeach; ?>
	
	    <a href="<?php echo $url ?>?page=<?php echo $results->getNextPage() ?>">&rsaquo;</a>
	    <a href="<?php echo $url ?>?page=<?php echo $results->getLastPage() ?>">&raquo;</a>
	  </div>
	<?php endif; ?>
	
	<?php echo SearchHelper::formatResults('Pieces', $results->getResults(), $format); ?>
	
	<?php if ($results->haveToPaginate()): ?>
	  <div class="pagination">
	    <a href="<?php echo $url ?>?page=1">&laquo;</a>
	    <a href="<?php echo $url ?>?page=<?php echo $results->getPreviousPage() ?>">&lsaquo;</a>
	
	    <?php foreach ($results->getLinks() as $page): ?>
	      <?php if ($page == $results->getPage()): ?>
	        <?php echo $page ?>
	      <?php else: ?>
	        <a href="<?php echo $url ?>?page=<?php echo $page ?>"><?php echo $page ?></a>
	      <?php endif; ?>
	    <?php endforeach; ?>
	
	    <a href="<?php echo $url ?>?page=<?php echo $results->getNextPage() ?>">&rsaquo;</a>
	    <a href="<?php echo $url ?>?page=<?php echo $results->getLastPage() ?>">&raquo;</a>
	  </div>
	<?php endif; ?>
</div>