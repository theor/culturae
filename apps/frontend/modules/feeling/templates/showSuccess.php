<table>
  <tbody>
    <tr>
      <th>Id:</th>
      <td><?php echo $feeling->getId() ?></td>
    </tr>
    <tr>
      <th>Value:</th>
      <td><?php echo $feeling->getValue() ?></td>
    </tr>
    <tr>
      <th>Comment:</th>
      <td><?php echo $feeling->getCommentId() ?></td>
    </tr>
    <tr>
      <th>Review:</th>
      <td><?php echo $feeling->getReviewId() ?></td>
    </tr>
    <tr>
      <th>Piece:</th>
      <td><?php echo $feeling->getPieceId() ?></td>
    </tr>
  </tbody>
</table>

<hr />

<a href="<?php echo url_for('feeling/edit?id='.$feeling->getId()) ?>">Edit</a>
&nbsp;
<a href="<?php echo url_for('feeling/index') ?>">List</a>
