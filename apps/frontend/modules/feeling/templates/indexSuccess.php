<h1>Feelings List</h1>

<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Value</th>
      <th>Comment</th>
      <th>Review</th>
      <th>Piece</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($feelings as $feeling): ?>
    <tr>
      <td><a href="<?php echo url_for('feeling/show?id='.$feeling->getId()) ?>"><?php echo $feeling->getId() ?></a></td>
      <td><?php echo $feeling->getValue() ?></td>
      <td><?php echo $feeling->getCommentId() ?></td>
      <td><?php echo $feeling->getReviewId() ?></td>
      <td><?php echo $feeling->getPieceId() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

  <a href="<?php echo url_for('feeling/new') ?>">New</a>
