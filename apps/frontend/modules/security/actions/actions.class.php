<?php

class securityActions extends sfActions
{
  public function executeLogin(sfWebRequest $request)
  {
    sfContext::getInstance()->getUser()->setAuthenticated(true);
    sfContext::getInstance()->getUser()->addCredential('admin');
  }

  public function executeLogout(sfWebRequest $req)
  {
    sfContext::getInstance()->getUser()->setAuthenticated(false);
    sfContext::getInstance()->getUser()->clearCredentials();

    sfContext::getInstance()->getUser()->getAttributeHolder()->removeNamespace('admin');
  }
}