<?php

/**
 * message actions.
 *
 * @package    culturae
 * @subpackage message
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class messageActions extends sfActions
{
    public function executeIndex(sfWebRequest $request)
    {
    	$this->getResponse()->setTitle('Culturae - Messages', false);
    	
        $this->url = ($request->getRelativeUrlRoot());

        $id = Auth::getUserLogged()->getId();

        $query = Doctrine_Query::create()
                        ->from("Message m")
                        ->where("author_id = " . $id)
                        ->orWhere("addressee_id = " . $id)
                        ->orderBy("m.id DESC");
        $this->messages = new sfDoctrinePager('Message', 8);
        $this->messages->setQuery($query);
        $this->messages->setPage($request->getParameter('page', 1));
        $this->messages->init();


        $qup = Doctrine_Query::create()->Update("Message m")
                ->set('isnew', '0')
                ->Where("addressee_id = " . $id)
                ->execute();
    }

    public function executeNew(sfWebRequest $request)
    {
    	$this->getResponse()->setTitle('Culturae - New message', false);
    	
        $this->addressee_id = $request->getParameter("addressee_id", -1);
        $this->addressee = Doctrine_core::getTable("User")->findOneById($this->addressee_id);

        $this->form = new MessageForm(array('addressee' => $this->addressee_id));

        if ($request->isMethod('post')) {
            $this->form->bind($request->getParameter('message'));
            if ($this->form->isValid()) {
                return $this->saveMessage($this->form->getValues());
            }
        }
    }

    public function saveMessage($values)
    {
        $id = $values["addressee"];
        $text = $values["text"];

        $user = Doctrine_core::getTable("User")->FindOneById($id);
        if ($user != NULL) {
            $message = new Message();
            $message->setAuthor(Auth::getUserLogged());
            $message->setAddressee($user);
            $message->setText($text);
            $message->setIsnew(1);
            $message->save();
        }
        return $this->redirect("message/index");
    }

}
