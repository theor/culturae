<h1>Messages</h1>

<?php 
	if (count($messages) == 0):
		echo "<p>You have no messages.</p>";
	endif;
?>

<?php foreach ($messages as $message): ?>
	<?php if ($message->getAuthorId() == Auth::getUserLogged()->getId()): ?>
                <div class="answer">
			<img class="avatar" src="<?php echo $message->getAuthor()->getAvatarPath() ?>" />
			
			<div class="content">
				<h4>
					<a href="<?php echo url_for('profile/view?id=' . $message->getAuthor()->getId()); ?>"><?php echo $message->getAuthor()->getFullname(); ?></a>
				</h4>
				<p><?php echo $message->getText(); ?></p>
			</div>
			
			<p class="date">Send: <?php echo $message->getCreatedAt(); ?></p>	
		</div>
	<?php else: ?>
		<div class="message <?php if($message->isnew() == 1) echo 'unread' ?>">
			<img class="avatar" src="<?php echo $message->getAuthor()->getAvatarPath() ?>" />
			
			<a class="answer like_button" href="<?php echo url_for('message/new?addressee_id=' . $message->getAuthor()->getId()); ?>">Answer</a>
			
			<div class="content">
				<h4>
					<a href="<?php echo url_for('profile/view?id=' . $message->getAuthor()->getId()); ?>"><?php echo $message->getAuthor()->getFullname(); ?></a>
				</h4>
				<p><?php echo $message->getText(); ?></p>
			</div>
			
			<p class="date">Received: <?php echo $message->getCreatedAt(); ?></p>	
		</div>
	<?php endif; ?>

<?php endforeach; ?>

<?php if ($messages->haveToPaginate()): ?>
<div class="pagination_desc">
  <strong><?php echo count($messages) ?></strong> messages

  <?php if ($messages->haveToPaginate()): ?>
    - page <strong><?php echo $messages->getPage() ?>/<?php echo $messages->getLastPage() ?></strong>
  <?php endif; ?>
</div>
  <div id="pagination">
    <a href="<?php echo $url ?>?page=1">&laquo;</a>
    <a href="<?php echo $url ?>?page=<?php echo $messages->getPreviousPage() ?>">&lsaquo;</a>

    <?php foreach ($messages->getLinks() as $page): ?>
      <?php if ($page == $messages->getPage()): ?>
        <?php echo $page ?>
      <?php else: ?>
        <a href="<?php echo $url ?>?page=<?php echo $page ?>"><?php echo $page ?></a>
      <?php endif; ?>
    <?php endforeach; ?>

    <a href="<?php echo $url ?>?page=<?php echo $messages->getNextPage() ?>">&rsaquo;</a>
    <a href="<?php echo $url ?>?page=<?php echo $messages->getLastPage() ?>">&raquo;</a>
  </div>
<?php endif; ?>