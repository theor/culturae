<h1>New Message</h1>

<div id="message_form">
	
	<form action="<?php echo url_for('message/new') ?>" method="POST">
	  <table>
	  	<tr>
	  		<td colspan="2" class="message_addressee">
	  			<img class="avatar" src="<?php echo $addressee->getAvatarPath(); ?>" alt="Contact avatar" />
	  			To: <a href="<?php echo url_for('profile/view?id=' . $addressee->getId()); ?>"><?php echo $addressee->getFullname(); ?></a>
			</td>
	  	</tr>
	    <?php echo $form ?>
	    <tr>
	      <td colspan="2" align="right">
	        <input type="submit" value="Send message" />
	      </td>
	    </tr>
	  </table>
	</form>
	<?php if ($sf_user->hasFlash('error_message')): ?>
	  <div class="flash_notice"><?php echo $sf_user->getFlash('error_message') ?></div>
	<?php endif ?>
</div>