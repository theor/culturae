<?php

/**
 * review actions.
 *
 * @package    culturae
 * @subpackage review
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class reviewActions extends sfActions
{
    public function executeIndex(sfWebRequest $request)
    {
        $this->reviews = Doctrine_Core::getTable('Review')
                        ->createQuery('a')
                        ->execute();
    }

    public function executeShow(sfWebRequest $request)
    {
        $this->review = Doctrine_Core::getTable('Review')->find(array($request->getParameter('id')));
        $this->forward404Unless($this->review);
    }

    public function executeNew(sfWebRequest $request)
    {
    	$this->getResponse()->setTitle('Culturae - New review', false);
    	
        $id = $request->getParameter('piece', null);
        $this->forward404If($id == null);

        $this->piece = Doctrine_Core::getTable('Piece')->findOneById($id);
        $this->form = new ReviewForm(null, array('piece_id' => $id));
    }

    public function executeCreate(sfWebRequest $request)
    {
        $this->forward404Unless($request->isMethod(sfRequest::POST));

        $piece_id = $request->getParameter('piece', -1);
        $this->form = new ReviewForm();

        $this->processForm($request, $this->form);

        $this->piece = Doctrine_Core::getTable('Piece')->findOneById($piece_id);

        $this->setTemplate('new');

        //$this->redirect("review/new?piece=" . $piece_id);
    }

    public function executeEdit(sfWebRequest $request)
    {
    	$this->getResponse()->setTitle('Culturae - Edit review', false);
    	
        $this->forward404Unless($review = Doctrine_Core::getTable('Review')->find(array($request->getParameter('id'))), sprintf('Object review does not exist (%s).', $request->getParameter('id')));

        $piece_id = $request->getParameter('piece', -1);
        $this->piece = Doctrine_Core::getTable('Piece')->findOneById($piece_id);
        $this->form = new ReviewForm($review, array('piece_id' => $piece_id));
    }

    public function executeUpdate(sfWebRequest $request)
    {
        $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));
        $this->forward404Unless($review = Doctrine_Core::getTable('Review')->find(array($request->getParameter('id'))), sprintf('Object review does not exist (%s).', $request->getParameter('id')));
        $this->form = new ReviewForm($review);

        $this->processForm($request, $this->form);

        $piece_id = $request->getParameter('piece', -1);
        $this->piece = Doctrine_Core::getTable('Piece')->findOneById($piece_id);

        $this->setTemplate('edit');
    }

    public function executeDelete(sfWebRequest $request)
    {
        $request->checkCSRFProtection();

        $this->forward404Unless($review = Doctrine_Core::getTable('Review')->find(array($request->getParameter('id'))), sprintf('Object review does not exist (%s).', $request->getParameter('id')));
        
        $url = "piece/show?id=" . $review->getPiece()->getId();
                
        $review->delete();

        $this->redirect($url);
    }

    public function executeLikers(sfWebRequest $request)
    {
        $this->likers = Doctrine_Core::getTable('Review')->findOneById($request->getParameter('id'))->getLikers();
    }

    protected function processForm(sfWebRequest $request, sfForm $form)
    {
        $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));

        if ($form->isValid()) {
            $review = $form->save();

            $this->redirect('piece/show?id=' . $form->getValue('piece_id'));
        }
    }

    public function executeLike(sfWebRequest $request)
    {
        $this->likeDislike(true, $request);
    }

    public function executeDislike(sfWebRequest $request)
    {
        $this->likeDislike(false, $request);
    }

    private function likeDislike($isLike, sfWebRequest $request)
    {
        $id = $request->getParameter('id');
        $this->piece = Doctrine_Core::getTable('Review')->findOneById($id);
        $this->forward404Unless($this->piece);

        $like = Doctrine_Core::getTable('Feeling')->findOneByUserIdAndReviewId(Auth::getUserLogged()->getId(), $this->piece->getId());

        if ($like == null) {
            $like = new Feeling();
            $like->setReview($this->piece);
            $like->setUser(Auth::getUserLogged());
        } else {
            if ($like->getValue() == ($isLike ? 0 : 1)) {
                $like->delete();
                return $this->redirect('piece/show?id=' . $this->piece->getPieceId());
            }
        }

        $like->setValue($isLike ? 0 : 1);
        $like->save();
        $this->redirect('piece/show?id=' . $this->piece->getPieceId());
    }

}
