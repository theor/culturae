<table>
  <tbody>
    <tr>
      <th>Id:</th>
      <td><?php echo $review->getId() ?></td>
    </tr>
    <tr>
      <th>Grade:</th>
      <td><?php echo $review->getGrade() ?></td>
    </tr>
    <tr>
      <th>Description:</th>
      <td><?php echo $review->getDescription() ?></td>
    </tr>
    <tr>
      <th>User:</th>
      <td><?php echo $review->getUserId() ?></td>
    </tr>
    <tr>
      <th>Piece:</th>
      <td><?php echo $review->getPieceId() ?></td>
    </tr>
  </tbody>
</table>

<hr />

<a href="<?php echo url_for('review/edit?id='.$review->getId()) ?>">Edit</a>
&nbsp;
<a href="<?php echo url_for('review/index') ?>">List</a>
