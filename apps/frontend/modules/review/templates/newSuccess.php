<?php use_javascript('review.js') ?>
<h1>Write a Review about '<?php echo $piece->getTitle() ?>'</h1>

<?php include_partial('form', array('form' => $form, 'piece' => $piece)) ?>

<?php echo link_to('Back to ' . $piece->getTitle(), 'piece/show?id=' . $piece->getId(), 'class=action_button') ?>