<h1>Reviews List</h1>

<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Grade</th>
      <th>Description</th>
      <th>User</th>
      <th>Piece</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($reviews as $review): ?>
    <tr>
      <td><a href="<?php echo url_for('review/show?id='.$review->getId()) ?>"><?php echo $review->getId() ?></a></td>
      <td><?php echo $review->getGrade() ?></td>
      <td><?php echo $review->getDescription() ?></td>
      <td><?php echo $review->getUserId() ?></td>
      <td><?php echo $review->getPieceId() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

  <a href="<?php echo url_for('review/new') ?>">New</a>
