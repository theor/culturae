<h1>I am ...</h1>

<div id="login_form">
	<h2>A registered user !</h2>
	<form action="<?php echo url_for('home/index?do=login') ?>" method="POST">
	  <table>
	    <?php echo $loginForm ?>
	    <tr>
	      <td colspan="2" align="right">
	        <input class="submitButton" type="submit" value="Login" />
	      </td>
	    </tr>
	  </table>
	</form>
	<?php if ($sf_user->hasFlash('error_login')): ?>
	  <div class="flash_notice"><?php echo $sf_user->getFlash('error_login') ?></div>
	<?php endif ?>	
</div>

<div id="register_form">
	<h2>A new user !</h2>
	<form action="<?php echo url_for('home/index?do=register') ?>" method="POST">
	  <table>
	    <?php echo $registerForm ?>
	    <tr>
	      <td colspan="2" align="right">
	        <input class="submitButton" type="submit" value="Register" />
	      </td>
	    </tr>
	  </table>
	</form>
	
    <?php if ($sf_user->hasFlash('error_register')): ?>
	  <div class="flash_notice"><?php echo $sf_user->getFlash('error_register') ?></div>
	<?php endif ?>
</div>

<div id="sign_in">

	<?php $token = 'http://' . $_SERVER['HTTP_HOST'] . '/';	?>

	<iframe 
		src="http://culturae.rpxnow.com/openid/embed?token_url=<?php echo urlencode($token); ?>" 
		scrolling="no" 
		frameBorder="0" 
		style="width:400px; height:240px">
	</iframe>
</div>