<?php

class homeActions extends sfActions
{
	public function executeIndex(sfWebRequest $request)
	{
		$this->getResponse()->setTitle('Culturae - Home', false);
		$userInfos = array();

		if (sfContext::getInstance()->getUser()->hasAttribute("register_auth_info"))
		{
			$ai = sfContext::getInstance()->getUser()->getAttribute("register_auth_info");
			sfContext::getInstance()->getUser()->getAttribute("register_auth_info");

			$userInfos = array();
			$userInfos['avatar'] = 'noprofile.jpg';
			$userInfos['sso_id'] = $ai['profile']['identifier'];

			switch($ai['profile']['providerName'])
			{
				case "Twitter":
					$userInfos['firstname'] = $ai['profile']['name']['formatted'];
					$userInfos['lastname'] = $ai['profile']['name']['formatted'];
					$userInfos['avatar'] = $ai['profile']['photo'];
					break;

				case "Facebook":
					$userInfos['avatar'] = $ai['profile']['photo'];

				case "Google":
					$userInfos['firstname'] = $ai['profile']['name']['givenName'];
					$userInfos['lastname'] = $ai['profile']['name']['familyName'];
					$userInfos['mail'] = $ai['profile']['email'];
					break;
			}

			sfContext::getInstance()->getUser()->setAttribute("register_auth_info", null);
		}

		$this->loginForm = new LoginForm();
		$this->registerForm = new RegisterForm($userInfos);
			

		$this->redirectIf(Auth::isLogged(), $this->generateUrl("default", array("module" => "newsfeed")));

		if ($request->isMethod('post'))
		{
			$token = Auth::getJanrainToken($request);
			if ($token != null)
			{
				sfContext::getInstance()->getUser()->setAttribute("jrtoken", $token);

				$url = $this->generateUrl("default", array("module" => "home", "action" => "login"));
				return $this->redirect($url);
			}
			else
			{
				if ($request->getGetParameter("do", null) == "login")
				{
					$this->loginForm->bind($request->getParameter('login'));
					if ($this->loginForm->isValid())
					{
						$postVars = $this->loginForm->getValues();
						sfContext::getInstance()->getUser()->setAttribute("mail", $postVars["mail"]);
						sfContext::getInstance()->getUser()->setAttribute("password", $postVars["password"]);
	
						$url = $this->generateUrl("default", array("module" => "home", "action" => "login"));
						return $this->redirect($url);
					}
				}
				else if ($request->getGetParameter("do", null) == "register")
				{
					$this->registerForm->bind($request->getParameter('register'));
					if ($this->registerForm->isValid())
					{
						$postVars = $this->registerForm->getValues();
						sfContext::getInstance()->getUser()->setAttribute("firstname", $postVars["firstname"]);
						sfContext::getInstance()->getUser()->setAttribute("lastname", $postVars["lastname"]);
						sfContext::getInstance()->getUser()->setAttribute("mail", $postVars["mail"]);
						sfContext::getInstance()->getUser()->setAttribute("password", $postVars["password"]);
						sfContext::getInstance()->getUser()->setAttribute("confirmPassword", $postVars["confirmPassword"]);
						sfContext::getInstance()->getUser()->setAttribute("sso", $postVars["sso_id"]);
						sfContext::getInstance()->getUser()->setAttribute("avatar", $postVars["avatar"]);
	
						$url = $this->generateUrl("default", array("module" => "home", "action" => "register"));
						return $this->redirect($url);
					}
				}
			}
		}
	}

	public function executeLogin(sfWebRequest $request)
	{
		$login = sfContext::getInstance()->getUser()->getAttribute("mail", null);
		$password = sfContext::getInstance()->getUser()->getAttribute("password", null);

		if ($login == null || $password == null)
		{
			$token = sfContext::getInstance()->getUser()->getAttribute("jrtoken", null);
			sfContext::getInstance()->getUser()->setAttribute("token", null);

			if ($token != null)
			{
				$auth_info = Auth::jrAuth($token);
				if ($auth_info == null)
				{
					$this->getUser()->setFlash('error_login', 'JanRain login failed (your social network account is not correctly bound) !');
					return $this->redirect($this->generateUrl("default", array("module" => "home", "action" => "index")));
				}

				if (Auth::jrConnect($auth_info) == null)
				{
					$this->getUser()->setFlash('error_register', 'Your account is valid !');

					sfContext::getInstance()->getUser()->setAttribute("register_auth_info", $auth_info);
					return $this->redirect($this->generateUrl("default", array("module" => "home", "action" => "index")));
				}
			}
		}
		else
		{
			$user = Auth::connect($login, $password);
			sfContext::getInstance()->getUser()->setAttribute("mail", null);
			sfContext::getInstance()->getUser()->setAttribute("password", null);
			// Login fail
			if ($user == NULL)
			{
				$this->getUser()->setFlash('error_login', 'Login failed !');

				$url = $this->generateUrl("default", array("module" => "home", "action" => "index"));
				return $this->redirect($url);
			}
		}

		$url = $this->generateUrl("default", array("module" => "newsfeed", "action" => "index"));
		return $this->redirect($url);
	}

	public function executeLogout(sfWebRequest $req)
	{
		Auth::disconnect();

		$url = $this->generateUrl("default", array("module" => "home", "action" => "index"));
		return $this->redirect($url);
	}

	public function executeRegister(sfWebRequest $request)
	{
		$login = sfContext::getInstance()->getUser()->getAttribute("mail", "");
		$password = sfContext::getInstance()->getUser()->getAttribute("password", "");
		$password_confirm = sfContext::getInstance()->getUser()->getAttribute("confirmPassword", "");
		$firstname = sfContext::getInstance()->getUser()->getAttribute("firstname", "");
		$lastname = sfContext::getInstance()->getUser()->getAttribute("lastname", "");
		$avatar = sfContext::getInstance()->getUser()->getAttribute("avatar", "noprofile.jpg");
		$sso = sfContext::getInstance()->getUser()->getAttribute("sso", "");

		if ($login == "" || $password == "" || $password_confirm == "" || $firstname == "" || $lastname == "")
		{	
			$this->getUser()->setFlash('error_register', 'You must fill all fields !');

			$url = $this->generateUrl("default", array("module" => "home", "action" => "index"));
			
			$this->unsetAttributes();
			
			return $this->redirect($url);
		}

		if ($password != $password_confirm)
		{
			$this->getUser()->setFlash('error_register', 'Please re-type your password correctly !');

			$url = $this->generateUrl("default", array("module" => "home", "action" => "index"));
			
			$this->unsetAttributes();
			
			return $this->redirect($url);
		}

		$checkUser = Doctrine_Core::getTable("User")->findOneByMail($login);
		if ($checkUser != NULL)
		{
			$this->getUser()->setFlash('error_register', 'This mail is already used by another user !');

			$url = $this->generateUrl("default", array("module" => "home", "action" => "index"));
			
			$this->unsetAttributes();
			
			return $this->redirect($url);
		}

		$user = new User();
		$user->setMail($login);
		$user->setPass(sha1($password));
		$user->setFirstname($firstname);
		$user->setLastname($lastname);
		$user->setAvatar($avatar);
		$user->setSsoid($sso);
		$user->save();

		sfContext::getInstance()->getUser()->setAuthenticated(true);
		sfContext::getInstance()->getUser()->addCredential('admin');
		sfContext::getInstance()->getUser()->setAttribute("user", $user);

		$url = $this->generateUrl("default", array("module" => "newsfeed", "action" => "index"));
		return $this->redirect($url);
	}

	private function unsetAttributes()
	{
		sfContext::getInstance()->getUser()->setAttribute("mail", null);
		sfContext::getInstance()->getUser()->setAttribute("password", null);
		sfContext::getInstance()->getUser()->setAttribute("confirmPassword", null);
		sfContext::getInstance()->getUser()->setAttribute("firstname", null);
		sfContext::getInstance()->getUser()->setAttribute("lastname", null);
		sfContext::getInstance()->getUser()->setAttribute("avatar", null);
		sfContext::getInstance()->getUser()->setAttribute("sso", null);
		
		sfContext::getInstance()->getUser()->setAttribute("jrtoken", null);
		sfContext::getInstance()->getUser()->setAttribute("token", null);
	}
}