<table>
  <tbody>
    <tr>
      <th>Id:</th>
      <td><?php echo $user->getId() ?></td>
    </tr>
    <tr>
      <th>Mail:</th>
      <td><?php echo $user->getMail() ?></td>
    </tr>
    <tr>
      <th>Pass:</th>
      <td><?php echo $user->getPass() ?></td>
    </tr>
    <tr>
      <th>Firstname:</th>
      <td><?php echo $user->getFirstname() ?></td>
    </tr>
    <tr>
      <th>Lastname:</th>
      <td><?php echo $user->getLastname() ?></td>
    </tr>
    <tr>
      <th>Avatar:</th>
      <td><?php echo $user->getAvatar() ?></td>
    </tr>
  </tbody>
</table>

<hr />

<a href="<?php echo url_for('user/edit?id='.$user->getId()) ?>">Edit</a>
&nbsp;
<a href="<?php echo url_for('user/index') ?>">List</a>
