<h1>Users List</h1>

<table>
  <thead>
    <tr>
      <th>Id</th>
      <th>Mail</th>
      <th>Pass</th>
      <th>Firstname</th>
      <th>Lastname</th>
      <th>Avatar</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($users as $user): ?>
    <tr>
      <td><a href="<?php echo url_for('user/show?id='.$user->getId()) ?>"><?php echo $user->getId() ?></a></td>
      <td><?php echo $user->getMail() ?></td>
      <td><?php echo $user->getPass() ?></td>
      <td><?php echo $user->getFirstname() ?></td>
      <td><?php echo $user->getLastname() ?></td>
      <td><?php echo $user->getAvatar() ?></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

  <a href="<?php echo url_for('user/new') ?>">New</a>
