<?php use_javascript('likers.js') ?>
<h1>Newsfeed</h1>
<ul>
    <?php if ($feed != NULL && count($feed) > 0):
        foreach ($feed as $x) : ?>
            <li><?php echo feedHelper::format($x) ?></li>
        <?php
        endforeach;
    else: ?>
            <p>There's nothing to display in your NewsFeed.</p>
            <p>Go rating and liking something !</p>
    <?php endif; ?>
</ul>