<?php

/**
 * feeling actions.
 *
 * @package    culturae
 * @subpackage feeling
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class newsfeedActions extends sfActions
{
    public function executeIndex(sfWebRequest $request)
    {
    	$this->getResponse()->setTitle('Culturae - Newsfeed', false);
    	
        $user = Auth::getUserLogged();
        $this->feed = array();
        $likes = Doctrine_Query::create()
                        ->select('*')
                        ->from('Feeling f')
                        ->where('f.user_id IN (SELECT pr.prescriptor_id FROM PrescriptorReference pr WHERE pr.follower_id = ?)', $user->getId())
                        ->orderBy('f.updated_at DESC')
                        ->execute();
        foreach ($likes as $like)
            array_push($this->feed, new News(News::FEELING, $like, $like->getUpdatedAt()));

        $reviews = Doctrine_Query::create()
                        ->from('Review r')
                        ->where('r.user_id IN (SELECT pr.prescriptor_id FROM PrescriptorReference pr WHERE pr.follower_id = ?)', $user->getId())
                        ->orderBy('r.created_at DESC')
                        ->execute();
        foreach ($reviews as $rev)
            array_push($this->feed, new News(News::REVIEW, $rev, $rev->getCreatedAt(), FeelingsHelper::GetLikersDisplay($rev, $this->getController(), 'review')));

        $comments = Doctrine_Query::create()
                        ->from('Comment f')
                        ->where('f.user_id IN (SELECT pr.prescriptor_id FROM PrescriptorReference pr WHERE pr.follower_id = ?)', $user->getId())
                        ->orderBy('f.created_at DESC')
                        ->execute();
        
        foreach ($comments as $com)
            array_push($this->feed, new News(News::COMMENT, $com, $com->getCreatedAt(), FeelingsHelper::GetLikersDisplay($com, $this->getController(), 'comment')));

        usort($this->feed, "feedHelper::cmp_news");
    }
}

