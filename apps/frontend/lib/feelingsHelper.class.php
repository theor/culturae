<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LinkHelper
 *
 * @author theo
 */
class FeelingsHelper
{
	public static function GetLikersDisplay($data, $controller, $type)
	{
		$user = Auth::getUserLogged();

		$knownLikers = $data->getKnownLikers($user);
		$unknownLikers = $data->getUnknownLikers($user);

		$displayLikers = array();
		$countKnownLikers = count($knownLikers);
		if ($countKnownLikers > 3) 
		{
			$nbDisplayUser = 3;
			for ($i = 1; $i < 4; $i++)
			{
				$liker = $knownLikers[$countKnownLikers - $i];
				$displayLiker = array('name' 	=>		$liker->getUser()->getFullname(),
                    				  'url' 	=> 		$controller->genUrl("profile/view?id=" . $liker->getUser()->getId()),
									  'class'	=>		'');
				array_push($displayLikers, $displayLiker);
			}
		} 
		else 
		{
			$nbDisplayUser = $countKnownLikers;
			for ($i = 1; $i <= $countKnownLikers; $i++)
			{
				$liker = $knownLikers[$countKnownLikers - $i];
				$displayLiker = array('name' 	=> 		$liker->getUser()->getFullname(),
                    				  'url' 	=> 		$controller->genUrl("profile/view?id=" . $liker->getUser()->getId()),
									  'class'	=>		'');
				array_push($displayLikers, $displayLiker);
			}
		}

		if (count($unknownLikers) > 0 || $countKnownLikers > 3) 
		{
			$count = count($unknownLikers) + $countKnownLikers - $nbDisplayUser;
			if ($count > 1) 
			{
				$displayLiker = array('name' 	=> 		$count . ' other people',
                    				  'url' 	=> 		$controller->genUrl($type . '/likers?id=' . $data->getId()),
									  'class'	=>		'likeLink');
			} 
			else 
			{
				$displayLiker = array('name' 	=> 		'1 other user',
                    				  'url' 	=> 		$controller->genUrl($type . '/likers?id=' . $data->getId()),
									  'class'	=>		'likeLink');
			}
			array_push($displayLikers, $displayLiker);
		}
		return ($displayLikers);
	}

	public static function GetLikersListDisplay($likers)
	{
		$display = "";
			
		$likers_count = count($likers);
		if ($likers_count > 0)
		{
			for ($k = 0; $k < $likers_count; $k++)
			{
				$liker = $likers[$k];

				if ($k != 0)
				{
					if ($k < $likers_count - 1)
					$display .= ", ";
					else
					$display .= " and ";
				}
				$display .= "<a class=\"" . $liker['class'] . "\" href=\"" . $liker['url'] . "\">" . $liker['name'] . "</a>";
			}
			$display .= " like.";
		}

		return $display;
	}
}

?>