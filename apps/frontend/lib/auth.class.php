<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Auth
 *
 * @author theo
 */
class Auth
{
    public static function connect($login, $pass)
    {
        $user = Doctrine_Core::getTable("User")->findOneByMailAndPass($login, sha1($pass));

        if ($user == null)
            return null;

        Auth::grantUser($user);
        return $user;
    }

    private static function grantUser($user)
    {
        sfContext::getInstance()->getUser()->setAuthenticated(true);
        sfContext::getInstance()->getUser()->addCredential('admin');
        sfContext::getInstance()->getUser()->setAttribute("user", $user);
        sfContext::getInstance()->getUser()->setUser($user);
    }

    public static function disconnect()
    {
        sfContext::getInstance()->getUser()->setAuthenticated(false);
        sfContext::getInstance()->getUser()->clearCredentials();
        sfContext::getInstance()->getUser()->getAttributeHolder()->removeNamespace('admin');
        sfContext::getInstance()->getUser()->setUser(null);
        sfContext::getInstance()->getUser()->setAttribute("auth_info", null);
        //sfContext::getInstance()->getUser()->setAttribute("user", null);
    }

    public static function isLogged()
    {
        return sfContext::getInstance()->getUser()->isAuthenticated();
    }

    public static function getUserLogged()
    {
        return sfContext::getInstance()->getUser()->getUser();
    }

    const JRAPIKEY = '46c345bafbf4b80671a66d6ac5b64d28d117bb15';
    public static function getJanrainToken($request)
    {
        return $request->getPostParameter('token', null);
    }

    public static function jrConnect($auth_info)
    {
        $q = Doctrine_Query::create()->from('User u')
                        ->where("u.ssoid LIKE '%". $auth_info['profile']['identifier'] ."%'")
                        ->execute();
        if ($q == null || count($q) == 0)
            return null;
        $user = $q[0];
        Auth::grantUser($user);
        sfContext::getInstance()->getUser()->setAttribute("auth_info", $auth_info);
        return $user;
    }

    public static function jrAuth($token)
    {
        $post_data = array('token' => $token,
            'apiKey' => Auth::JRAPIKEY,
            'format' => 'json',
            'extended' => 'false'); //Extended is not available to Basic.

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_URL, 'https://rpxnow.com/api/v2/auth_info');
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_FAILONERROR, true);
        $result = curl_exec($curl);
        if ($result == false) {
            echo "\n" . 'Curl error: ' . curl_error($curl);
            echo "\n" . 'HTTP code: ' . curl_errno($curl);
            echo "\n";
            var_dump($post_data);
        }
        curl_close($curl);


        /* STEP 3: Parse the JSON auth_info response */
        $auth_info = json_decode($result, true);

        if ($auth_info['stat'] == 'ok') {
            return $auth_info;
//            echo "\n auth_info:";
//            echo "\n";
//            print_r($auth_info);
        } else {
            return null;
            // Gracefully handle auth_info error.  Hook this into your native error handling system.
            echo "\n" . 'An error occured: ' . $auth_info['err']['msg'] . "\n";
            var_dump($auth_info);
            echo "\n";
            var_dump($result);
        }
    }

}

?>
