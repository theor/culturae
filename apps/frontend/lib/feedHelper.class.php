<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of feedHelper
 *
 * @author theo
 */
class feedHelper
{
	public static function cmp_news($a, $b)
	{
		$dateA = strtotime($a->date);
		$dateB = strtotime($b->date);
		return $dateA < $dateB;
	} 
	
    public static function formatLike($d, $f)
    {
        $res = "<div>";

        $res .= $f->getUser()->getFullname();
        $res .= $f->getValue() == Feeling::LIKE ? " likes " : " dislikes ";

        switch ($f->getKind()) {
            case Feeling::ONPIECE:
                $res .= $f->getPiece()->getTitle();
                break;
            case Feeling::ONREVIEW:
                $res .= $f->getReview()->getUser()->getFullname();
                $res .= "'s review on " . $f->getReview()->getPiece()->getTitle();
                break;
            case Feeling::ONCOMMENT:
                $res .= $f->getComment()->getUser()->getFullname();
                $res .= "'s comment: " . substr($f->getComment()->getText(), 0, 20) . "...";
                break;
            default:
                break;
        }

        $res .= "</div>";
        
        $user = $f->getUser();
        $userId = $f->getUserId();
        $avatar = $user->getAvatarPath();
        $fullname = $user->getFullname();
        
        
        $r  = '<div class="review">' . "\n";
        $r .= ' <img class="avatar" src="' . $avatar . '" alt="' . $fullname . '" />' . "\n";
        $r .= '		<p class="name">' . "\n";
        $r .= '			<a href="' . url_for('profile/view?id=' . $userId) . '">' . $fullname . '</a>';
        $r .= $f->getValue() == Feeling::LIKE ? " likes " : " dislikes ";
        
    	switch ($f->getKind()) {
            case Feeling::ONPIECE:
                $piece = $f->getPiece();
		        $pieceId = $f->getPieceId();
		        $title = $piece->getTitle();
		        
                $r .= '<a href="' . url_for('piece/show?id=' . $pieceId) . '">' . $title . "</a>\n";
                break;
            case Feeling::ONREVIEW:
		        $review = $f->getReview();
		        $title = $review->getPiece()->getTitle();
		        $pieceId = $review->getPieceId();
        		$reviewerId = $review->getUserId();
        		$reviewer = $review->getUser()->getFullname();
		        
		        $r .= '<a href="' . url_for('profile/view?id=' . $reviewerId) . '">' . $reviewer;
        		$r .= '</a>\'s review about ' . '<a href="' . url_for('piece/show?id=' . $pieceId) . '">' . $title . "</a>\n";
                break;
            case Feeling::ONCOMMENT:
            	$comment = $f->getComment();
            	$commentId = $f->getCommentId();
            	$commenter = $comment->getUser()->getFullname();
            	$commenterId = $comment->getUserId();
            	$commentText = $f->getComment()->getText();
            	$commentText = substr($commentText, 0, 20) . '...';
            	
            	$commentReview = $comment->getReview();
            	$commentReviewAuthor = $commentReview->getUser();
            	
            	$r .= '<a href="' . url_for('profile/view?id=' . $commenterId) . '">';
		        $r .= $commenter;
        		$r .= '</a>\'s comment on ';
        		$r .= '<a href="' . url_for('piece/show?id=' . $commentReview->getPiece()->getId() .'&viewAllReviews=1#review' . $commentReview->getId()) . '">' . $commentReviewAuthor->getFullname() . '\'s review</a> about ' . $comment->getReview()->getPiece()->getTitle();
                $r .= ': "';
        		$r .= $commentText;
        		
//        		$r .= '			<a href="' . url_for('profile/view?id=' . $commenterId) . '">';
//		        $r .= $commenter;
//        		$r .= '</a>\'s comment on ';
//                $r .= '			<a href="' . url_for('comment/show?id=' . $commentId) . '">';
//        		$r .= $commentText;
//        		$r .= "			</a>\n";
        		
                break;
            default:
                break;
        }
        
        $r .= '		</p>' . "\n";
        $r .= ' <div class="newsDate">'.$d.'</div>';
        $r .= '</div>' . "\n";
        
        return $r;
    }

    public static function formatComment($d, $f, $likers)
    {
		$limit = 40;
        $userId = $f->getUserId();
        $user = $f->getUser();
        $review = $f->getReview();
        $avatar = $user->getAvatarPath();
        $fullname = $user->getFullname();
        $reviewerId = $review->getUserId();
        $pieceId = $review->getPieceId();
        $reviewer = $review->getUser()->getFullname();
        $title = $review->getPiece()->getTitle();
    	$desc = $f->getText();
        $desc = str_replace('&amp;', '&', $desc);
		$desc = str_replace('&quot;', '"', $desc);
		$desc = str_replace('&#039;', "'", $desc);
		$desc = str_replace("\n", ' ', $desc);
		$desc = str_replace('  ', ' ', $desc);
		
    	if (strlen($desc) > $limit)
    	{
    		$desc = substr($desc, 0, $limit - 3);
    		$desc .= '...';
    	}
        
        $r  = '<div class="review">' . "\n";
        $r .= '	<img class="avatar" src="' . $avatar . '" alt="' . $fullname . '" />' . "\n";
        
        //$r .= '	<div class="content">' . "\n";
        
        $r .= '	<div class="likeZone">' . "\n";
		$r .= LinkHelper::getLikeLink('comment', $f) . "&nbsp;" . LinkHelper::getDislikeLink('comment', $f);		
        $r .= '	</div>' . "\n";
        
        $r .= '		<p class="name">' . "\n";
        $r .= '			<a href="' . url_for('profile/view?id=' . $userId) . '">';
        $r .= $fullname;
        $r .= '</a> just commented ';
        $r .= '<a href="' . url_for('profile/view?id=' . $reviewerId) . '">';
        $r .= $reviewer;
        $r .= '</a>\'s review about ';
        $r .= '<a href="' . url_for('piece/show?id=' . $pieceId) . '">';
        $r .= $title;
        $r .= "</a>\n";
        $r .= '		</p>' . "\n";
        $r .= '		<div class="message">"' . $desc . '"</div>' . "\n";
        //$r .= '</div>' . "\n";
		
        $r .= ' <div class="newsDate">';
        $r .= FeelingsHelper::GetLikersListDisplay($likers) . '<br />';
        $r .= $d;
        $r .= '</div>';
        $r .= '</div>' . "\n";
        
        return $r;
    }

    public static function formatReview($d, $f, $likers)
    {
		$limit = 40;
		$user = $f->getUser();
        $avatar = $user->getAvatarPath();
        $fullname = $user->getFullname();
        $title = $f->getPiece()->getTitle();
        $id = $f->getId();
        $grade = $f->getGrade();
        $userId = $f->getUserId();
        $pieceId = $f->getPieceId();
        $desc = $f->getDescription();
        $desc = str_replace('&amp;', '&', $desc);
		$desc = str_replace('&quot;', '"', $desc);
		$desc = str_replace('&#039;', "'", $desc);
		$desc = str_replace("\n", ' ', $desc);
		$desc = str_replace('  ', ' ', $desc);
		
    	if (strlen($desc) > $limit)
    	{
    		$desc = substr($desc, 0, $limit - 3);
    		$desc .= '...';
    	}
    	
        $r  = '<div class="review">' . "\n";
        $r .= '	<img class="avatar" src="' . $avatar . '" alt="' . $fullname . '" />' . "\n";
        
        $r .= '	<div class="likeZone">' . "\n";
		$r .= LinkHelper::getLikeLink('review', $f) . "&nbsp;" . LinkHelper::getDislikeLink('review', $f) . "&nbsp;";
		$r .= link_to('Comment', 'comment/new?review=' . $id, 'class=like_button') . "\n";		
        $r .= '	</div>' . "\n";
        
        $r .= '		<p class="name">' . "\n";
        $r .= '			<a name="review' . $id . '" href="' . url_for('profile/view?id=' . $userId) . '">';
        $r .= $fullname;
        $r .= '</a> just reviewed ';
        $r .= '<a href="' . url_for('piece/show?id=' . $pieceId) . '">';
        $r .= $title;
        $r .= "</a>\n";
        $r .= '</p>' . "\n";
        for ($i = 0; $i < $grade; $i++) 
        	$r .= '<img class="star" src="/images/star_o.png" alt="1" />';
		for ($i = $grade; $i < 5; $i++)
			$r .= '<img class="star" src="/images/star_n.png" alt="0" />';
        $r .= '		<p class="message">"' . $desc . '"</p>' . "\n";
        $r .= ' <div class="newsDate">';
        $r .= FeelingsHelper::GetLikersListDisplay($likers);
        $r .=  '<br />';
		$r .= $d;
        $r .= '</div>' . "\n";
        $r .= '</div>' . "\n";
        return $r;
    }

    public static function format($n)
    {
        switch ($n->kind) {
            case News::FEELING:
                return feedHelper::formatLike($n->date, $n->object);
            case News::COMMENT:
                return feedHelper::formatComment($n->date, $n->object, $n->likers);
            case News::REVIEW:
                return feedHelper::formatReview($n->date, $n->object, $n->likers);
            default:
                return "<div>Not implemented !</div>";
        }
    }
}

?>
