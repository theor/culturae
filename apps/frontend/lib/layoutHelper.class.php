<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of layoutHelper
 *
 * @author theo
 */
class layoutHelper {
    public static function getNumberOfUnreadMessaqgesOfCurrentUser()
    {
        $user = Auth::getUserLogged();
        return Doctrine_Core::getTable('Message')->findByAddresseeIdAndIsnew($user->getId(), 1)->count();
    }

    public static function formatMessagesNumber()
    {
        $nb = layoutHelper::getNumberOfUnreadMessaqgesOfCurrentUser();
        if ($nb == 0)
            return "";
        return "<span class='nbMessages'>". $nb ."</span>";
    }
}
?>
