<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LinkHelper
 *
 * @author theo
 */
class LinkHelper
{
    public static function getLikeLink($kind, $piece)
    {
        return link_to($piece->canLike() ? 'Like' : 'Unlike', $kind . '/like?id=' . $piece->getId(), 'class=like_button');
    }

    public static function getDislikeLink($kind, $piece)
    {
        return link_to($piece->canDislike() ? 'Dislike' : 'UnDislike', $kind . '/dislike?id=' . $piece->getId(), 'class=like_button');
    }

}

?>
