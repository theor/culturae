<?php

class myUser extends sfBasicSecurityUser
{
    protected $user;
    public function getUser()
    {
        return $this->getAttribute("user", NULL);
    }

    public function setUser($user)
    {
        $this->setAttribute("user", $user);
    }

    protected $ssoInfo;
    public function getSsoInfo()
    {
        return $this->ssoInfo;
    }

    public function setSsoInfo($ssoInfo)
    {
        $this->ssoInfo = $ssoInfo;
    }

}
