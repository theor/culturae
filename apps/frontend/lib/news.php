<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of news
 *
 * @author theo
 */
class News
{
    const REVIEW = 0;
    const FEELING = 1;
    const COMMENT = 2;

    public $kind;
    public $object;
    public $date;
    public $likers;
    
	public function __construct($kind, $object, $date, $likers = null)
    {
        if ($kind < 0 || $kind > 2)
            die($kind);
        $this->kind = $kind;
        $this->object = $object;
        $this->date = $date;
        $this->likers = $likers;
    }
}
?>
