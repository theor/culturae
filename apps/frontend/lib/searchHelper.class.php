<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SearchHelper
 *
 * @author theo
 */
class SearchHelper
{
    public static function formatPiece($p)
    {
    	$limit = 250;
    	
    	$genre = $p->getGenre();
    	$title = $p->getTitle();
    	$id = $p->getId();
    	$desc = $p->getDescription();
    	$image = $p->getImage();
    	$kind = $p->getKind();
    	
    	$reviews = Doctrine_Query::create()
					->from('Review r')
					->where("r.piece_id = $id")
					->execute();
		$grade = 0;
		$size = count($reviews);
		for ($k = 0; $k < $size; $k++)
		{
			$review = $reviews[$k];
			$grade += $review->getGrade();
		}
		$grade /= ( $size > 0 ? $size : 1);

		$desc = str_replace('&amp;', '&', $desc);
		$desc = str_replace('&lt;b&gt;', '', $desc);
		$desc = str_replace('&lt;/b&gt;', '', $desc);
		$desc = str_replace('&quot;', '"', $desc);
		$desc = str_replace('&#039;', "'", $desc);
		$desc = str_replace("\n", ' ', $desc);
		$desc = str_replace('  ', ' ', $desc);
		
    	if (strlen($desc) > $limit)
    	{
    		$desc = substr($desc, 0, $limit - 3);
    		$desc .= '...';
    	}
    	
    	$format  = '	<a class="resultLink" href="' . url_for("piece/show?id=$id") . "\">\n";
    	$format .= "		<li class=\"result\">\n";
		for ($i = $grade; $i < 5; $i++)
			$format .= '<img class="stars" src="/images/star_n.png" alt="0" />';
		for ($i = 0; $i < $grade; $i++)
			$format .= '<img class="stars" src="/images/star_o.png" alt="1" />';
    	$format .= "			<img class=\"piece\" src=\"$image\" alt=\"$title\" />\n";
    	$format .= "			<h2>$title</h2>\n";
    	$format .= "			<h3>$kind - $genre</h3>\n";
    	$format .= "			<p>$desc</p>\n";
    	$format .= "		</li>\n";
    	$format .= "	</a>\n";
    	
    	return $format;
    }

    public static function formatUser($u)
    {
    	$fullname = $u->getFullname();
    	$id = $u->getId();
    	
		$res  = '	<a class="resultLink" class="result" href="' . url_for("profile/view?id=$id") . '">';
    	$res .= "		<li class=\"result\">\n";
		$res .= "				$fullname";
    	$res .= "		</li>\n";
		$res .= '	</a>';
		return $res;
    }

    public static function formatAuthor($u)
    {
    	$name = $u->getName();
    	$id = $u->getId();
    	
        $res  = '	<a class="resultLink" href="' . url_for("author/show?id=$id") . '">';
    	$res .= "		<li class=\"result\">\n";
        $res .= "				$name";
    	$res .= "		</li>\n";
        $res .= '	</a> ';
    	return $res;
    }

    public static function formatResults($cat, $results, $funcDisplay)
    {
        //echo '<em>'. count($results) .'</em> in ' . $cat;
        $result = '';
        if (count($results) > 0)
        {
            $result .= "<ul id=\"results\">\n";
            foreach ($results as $res)
                $result .= SearchHelper::$funcDisplay($res);
            $result .= "</ul>\n";
        }
        return $result;
    }
}

?>
