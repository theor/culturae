<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of settingsHelper
 *
 * @author theo
 */
class settingsHelper
{
    public static function getAccountName($authinfo)
    {
        try
        {
            return $authinfo['profile']['displayName'];
        }
        catch (Exception $exc)
        {
            return "";
        }
    }

    public static function getSiteName($authinfo)
    {
        try
        {
            return $authinfo['profile']['providerName'];
        }
        catch (Exception $exc)
        {
            return "";
        }
    }

    public static function getSiteLogo($authinfo)
    {
        try
        {
            return "/images/" . strtolower(settingsHelper::getSiteName($authinfo)) . ".png";
        }
        catch (Exception $exc)
        {
            return "";
        }
    }
}

?>
