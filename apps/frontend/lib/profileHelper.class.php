<?php

class ProfileHelper
{
	public static function canSubscribe($id)
	{
		return Doctrine_Core::getTable('PrescriptorReference')->findOneByPrescriptorIdAndFollowerId($id, Auth::getUserLogged()->getId()) == NULL;
	}
	
	public static function canUnsubscribe($id)
	{
		return Doctrine_Core::getTable('PrescriptorReference')->findOneByPrescriptorIdAndFollowerId($id, Auth::getUserLogged()->getId()) != NULL;
	}
	
	public static function getLikes($user, $limit = -1)
	{
		$query = Doctrine_Query::create()
			  		->from("Feeling f")
			  		->where("f.user_id = " . $user->getId())
			  		->andWhere("f.value = " . Feeling::LIKE)
			  		->andWhere("f.piece_id IS NOT NULL")
			  		->orderBy("f.id DESC");
		if ($limit != -1)
			$query->limit($limit);
		return $query->execute();
	}
	
	public static function getDislikes($user, $limit = -1)
	{
		$query = Doctrine_Query::create()
			  		->from("Feeling f")
			  		->where("f.user_id = " . $user->getId())
			  		->andWhere("f.value = " . Feeling::DISLIKE)
			  		->andWhere("f.piece_id IS NOT NULL")
			  		->orderBy("f.id DESC");
		if ($limit != -1)
			$query->limit($limit);
		return $query->execute();
	}
	
	public static function getPrescribers($user, $limit = -1)
	{
		$query = Doctrine_query::create()
	  		->from("PrescriptorReference p")
	  		->where("p.follower_id = ?", $user->getId());

	  	if ($limit != -1)
			$query->limit($limit);
			
		$prescriptions = $query->execute();
	  	
	  	$prescribers = array();
	  	foreach($prescriptions as $prescription)
	  	{
	  		$prescribers[$prescription->getPrescriptorId()] = Doctrine_core::getTable("User")->findOneById($prescription->getPrescriptorId());
	  	}
	  	
	  	return $prescribers;
	}
	
	public static function getSuggestions($user, $limit = -1)
	{
		$prescriptions = Doctrine_query::create()
		  		->from("PrescriptorReference p")
		  		->where("p.follower_id = ?", $user->getId())
		  		->execute();	

		$friendsId = array();
	  	foreach($prescriptions as $prescription)
	  	{
	  		array_push($friendsId, $prescription->getPrescriptorId());
	  	}
	  	  	
	  	$myFeelingsList = Doctrine_Query::create()
	  		->from("Feeling f")
	  		->where("f.user_id = ?", $user->getId())
	  		->andWhere("f.piece_id IS NOT NULL")
	  		->execute();
	  		
	  	$myFeelings = array();
	  	foreach($myFeelingsList as $feeling)
	  	{
	  		array_push($myFeelings, $feeling->getPieceId());
	  	}
	  		
	  	$query = Doctrine_Query::create()
	  		->from("Feeling f")
	  		->where("f.value = ?", 0)
	  		->andWhere("f.piece_id IS NOT NULL")
	  		->andWhereIn("f.user_id", $friendsId)
	  		->andWhereNotIn("f.piece_id", $myFeelings)
	  		->groupBy("f.piece_id");
	  		
	  	if ($limit != -1)
			$query->limit($limit);
			
		return $query->execute();
	}
}

?>