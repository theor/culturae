<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <?php include_title() ?>
    <link rel="shortcut icon" href="/favicon.ico" />
    <?php include_stylesheets() ?>
    <?php use_javascript('jquery-1.5.2.js') ?>
    <?php use_javascript('search.js') ?>
  </head>
  <body>
  	<div id="header">
  		<p class="slogan">SHARE COMMENT DISCOVER ENJOY</p>
  		
  		<form id="searchForm" action="<?php echo url_for('search/index') ?>" method="post">
  			<img src="/images/v2/search.png" width="32" height="32" alt="search" />
  			<input id="search" name="search" type="text" />
  			<input type="submit" value="Search" /> 
  		</form>
  		
  		<?php 
  			$moduleName = $sf_context->getModuleName();
  			$actionName = $sf_context->getActionName();
  			
  		?>
  		
  		<ul id="menus">
	  		<li><a href="<?php echo url_for('profile/settings'); ?>" class="<?php echo ($moduleName == "profile" && $actionName == "settings") ? "selected" : "none" ?>">Settings</a></li>  
	  		<li><a href="<?php echo url_for('piece/new'); ?>" class="<?php echo ($moduleName == "piece" && $actionName == "new") ? "selected" : "none" ?>">New Piece</a></li>
                        <li><a href="<?php echo url_for('message/index'); ?>" class="<?php echo $moduleName == "message" ? "selected" : "none" ?>">Messages <?php echo layoutHelper::formatMessagesNumber(); ?></a></li>
	  		<li><a href="<?php echo url_for('profile/view?id=' . Auth::getUserLogged()->getId()); ?>" class="<?php echo ($moduleName == "profile" && $actionName == "view") ? "selected" : "none" ?>">Profile</a></li>
                        <li><a href="<?php echo url_for('newsfeed/index'); ?>" class="<?php echo $moduleName == "newsfeed" ? "selected" : "none" ?>">What's new</a></li>
  		</ul>
  	</div>
    
    <div id="bodyContent">
	    <div id="userPanel">
	    	<?php include_component('profile', 'profile') ?>
	    </div>    	
	    
	    <div id="content">
	    	<?php echo $sf_content ?>
	    </div>
	    
	    <div id="footer">
	    </div>
    </div>
    <?php include_javascripts() ?>  
  </body>
</html>
