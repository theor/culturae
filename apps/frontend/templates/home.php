<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <?php include_http_metas() ?>
        <?php include_metas() ?>
        <?php include_title() ?>
        <link rel="shortcut icon" href="/favicon.ico" />
        <?php include_stylesheets() ?>
        <?php include_javascripts() ?>
    </head>
    <body>
        <div id="header">
            <p>" Culturae is a social network based on culture.<br />
  		Share your feedbacks, write reviews, discover new things. "</p>
            <p class="slogan">SHARE COMMENT DISCOVER ENJOY</p>
        </div>

        <div id="content">
            <?php echo $sf_content ?>
        </div>
        <script type="text/javascript">
            var rpxJsHost = (("https:" == document.location.protocol) ? "https://" : "http://static.");
            document.write(unescape("%3Cscript src='" + rpxJsHost +
                "rpxnow.com/js/lib/rpx.js' type='text/javascript'%3E%3C/script%3E"));
        </script>
        <script type="text/javascript">
            RPXNOW.overlay = true;
            RPXNOW.language_preference = 'en';
        </script>
    </body>
</html>
