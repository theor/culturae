<!--
To change this template, choose Tools | Templates
and open the template in the editor.
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<p><?php echo count($likers) ?> people like:</p>
<ul>
    <?php foreach($likers as $l): ?>
    	<li>
    		<img src="<?php echo $l->getUser()->getAvatarPath(); ?>" width="35" height="25" alt="<?php echo $l->getUser()->getFullname(); ?>" />
	    	<a href="<?php echo url_for("profile/view?id=" . $l->getUser()->getId()); ?>"><?php echo $l->getUser()->getFullname(); ?></a>
    	</li>
    <?php endforeach; ?>
</ul>
<a class="action_button close" href="#">Back</a>
