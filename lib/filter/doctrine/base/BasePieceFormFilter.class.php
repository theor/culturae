<?php

/**
 * Piece filter form base class.
 *
 * @package    culturae
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BasePieceFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'title'       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'description' => new sfWidgetFormFilterInput(),
      'image'       => new sfWidgetFormFilterInput(),
      'kind'        => new sfWidgetFormChoice(array('choices' => array('' => '', 'Book' => 'Book', 'Movie' => 'Movie', 'Album' => 'Album'))),
      'genre'       => new sfWidgetFormChoice(array('choices' => array('' => '', 'SciFi' => 'SciFi', 'Horror' => 'Horror', 'Comedy' => 'Comedy'))),
      'author_id'   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Author'), 'add_empty' => true)),
    ));

    $this->setValidators(array(
      'title'       => new sfValidatorPass(array('required' => false)),
      'description' => new sfValidatorPass(array('required' => false)),
      'image'       => new sfValidatorPass(array('required' => false)),
      'kind'        => new sfValidatorChoice(array('required' => false, 'choices' => array('Book' => 'Book', 'Movie' => 'Movie', 'Album' => 'Album'))),
      'genre'       => new sfValidatorChoice(array('required' => false, 'choices' => array('SciFi' => 'SciFi', 'Horror' => 'Horror', 'Comedy' => 'Comedy'))),
      'author_id'   => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Author'), 'column' => 'id')),
    ));

    $this->widgetSchema->setNameFormat('piece_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Piece';
  }

  public function getFields()
  {
    return array(
      'id'          => 'Number',
      'title'       => 'Text',
      'description' => 'Text',
      'image'       => 'Text',
      'kind'        => 'Enum',
      'genre'       => 'Enum',
      'author_id'   => 'ForeignKey',
    );
  }
}
