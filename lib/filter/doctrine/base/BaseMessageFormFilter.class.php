<?php

/**
 * Message filter form base class.
 *
 * @package    culturae
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseMessageFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'text'         => new sfWidgetFormFilterInput(),
      'author_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Author'), 'add_empty' => true)),
      'addressee_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Addressee'), 'add_empty' => true)),
      'isnew'        => new sfWidgetFormChoice(array('choices' => array('' => '', 'False' => 'False', 'True' => 'True'))),
      'created_at'   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'text'         => new sfValidatorPass(array('required' => false)),
      'author_id'    => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Author'), 'column' => 'id')),
      'addressee_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Addressee'), 'column' => 'id')),
      'isnew'        => new sfValidatorChoice(array('required' => false, 'choices' => array('False' => 'False', 'True' => 'True'))),
      'created_at'   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('message_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Message';
  }

  public function getFields()
  {
    return array(
      'id'           => 'Number',
      'text'         => 'Text',
      'author_id'    => 'ForeignKey',
      'addressee_id' => 'ForeignKey',
      'isnew'        => 'Enum',
      'created_at'   => 'Date',
    );
  }
}
