<?php

/**
 * User filter form base class.
 *
 * @package    culturae
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseUserFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'mail'             => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'pass'             => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'firstname'        => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'lastname'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'avatar'           => new sfWidgetFormFilterInput(),
      'ssoid'            => new sfWidgetFormFilterInput(),
      'prescribers_list' => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'User')),
    ));

    $this->setValidators(array(
      'mail'             => new sfValidatorPass(array('required' => false)),
      'pass'             => new sfValidatorPass(array('required' => false)),
      'firstname'        => new sfValidatorPass(array('required' => false)),
      'lastname'         => new sfValidatorPass(array('required' => false)),
      'avatar'           => new sfValidatorPass(array('required' => false)),
      'ssoid'            => new sfValidatorPass(array('required' => false)),
      'prescribers_list' => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'User', 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('user_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function addPrescribersListColumnQuery(Doctrine_Query $query, $field, $values)
  {
    if (!is_array($values))
    {
      $values = array($values);
    }

    if (!count($values))
    {
      return;
    }

    $query
      ->leftJoin($query->getRootAlias().'.PrescriptorReference PrescriptorReference')
      ->andWhereIn('PrescriptorReference.prescriptor_id', $values)
    ;
  }

  public function getModelName()
  {
    return 'User';
  }

  public function getFields()
  {
    return array(
      'id'               => 'Number',
      'mail'             => 'Text',
      'pass'             => 'Text',
      'firstname'        => 'Text',
      'lastname'         => 'Text',
      'avatar'           => 'Text',
      'ssoid'            => 'Text',
      'prescribers_list' => 'ManyKey',
    );
  }
}
