<?php

/**
 * Piece
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @package    culturae
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7691 2011-02-04 15:43:29Z jwage $
 */
class Piece extends BasePiece
{
    public function canLike()
    {
        $f = Doctrine_Core::getTable('Feeling')->findOneByUserIdAndPieceId(Auth::getUserLogged()->getId(), $this->getId());
        return $f == null || $f->getValue() == 1;
    }

    public function canDislike()
    {
        $f = Doctrine_Core::getTable('Feeling')->findOneByUserIdAndPieceId(Auth::getUserLogged()->getId(), $this->getId());        
        
        return $f == null || $f->getValue() == 0;
    }
    
	public function getLikers()
    {
    	return Doctrine_Core::getTable('Feeling')->findByPieceIdAndValue($this->getId(), 0);
    }
    
	public function getUnknownLikers($user)
    {
    	$prescribers = ProfileHelper::getPrescribers($user);
    	
    	$prescribersList = array();
    	foreach($prescribers as $prescriber)
    	{
    		array_push($prescribersList, $prescriber->getId());
    	}
    	
    	return Doctrine_Query::create()
    		->from("Feeling f")
    		->where("f.piece_id = ?", $this->getId())
    		->andWhere("f.value = ?", 0)
    		->andWhereNotIn("f.user_id", $prescribersList)
    		->execute();
    }
    
	public function getKnownLikers($user)
    {
    	$prescribers = ProfileHelper::getPrescribers($user);
    	
    	$prescribersList = array();
    	foreach($prescribers as $prescriber)
    	{
    		array_push($prescribersList, $prescriber->getId());
    	}
    	
    	return Doctrine_Query::create()
    		->from("Feeling f")
    		->where("f.piece_id = ?", $this->getId())
    		->andWhere("f.value = ?", 0)
    		->andWhereIn("f.user_id", $prescribersList)
    		->execute();
    }
}
