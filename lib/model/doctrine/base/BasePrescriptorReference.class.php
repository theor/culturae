<?php

/**
 * BasePrescriptorReference
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $follower_id
 * @property integer $prescriptor_id
 * @property User $Follower
 * @property User $Prescriptor
 * 
 * @method integer              getFollowerId()     Returns the current record's "follower_id" value
 * @method integer              getPrescriptorId()  Returns the current record's "prescriptor_id" value
 * @method User                 getFollower()       Returns the current record's "Follower" value
 * @method User                 getPrescriptor()    Returns the current record's "Prescriptor" value
 * @method PrescriptorReference setFollowerId()     Sets the current record's "follower_id" value
 * @method PrescriptorReference setPrescriptorId()  Sets the current record's "prescriptor_id" value
 * @method PrescriptorReference setFollower()       Sets the current record's "Follower" value
 * @method PrescriptorReference setPrescriptor()    Sets the current record's "Prescriptor" value
 * 
 * @package    culturae
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7691 2011-02-04 15:43:29Z jwage $
 */
abstract class BasePrescriptorReference extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('prescriptor_reference');
        $this->hasColumn('follower_id', 'integer', null, array(
             'type' => 'integer',
             'primary' => true,
             ));
        $this->hasColumn('prescriptor_id', 'integer', null, array(
             'type' => 'integer',
             'primary' => true,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('User as Follower', array(
             'local' => 'follower_id',
             'foreign' => 'id',
             'onDelete' => 'CASCADE'));

        $this->hasOne('User as Prescriptor', array(
             'local' => 'prescriptor_id',
             'foreign' => 'id',
             'onDelete' => 'CASCADE'));

        $timestampable0 = new Doctrine_Template_Timestampable(array(
             'updated' => 
             array(
              'disabled' => true,
             ),
             ));
        $this->actAs($timestampable0);
    }
}