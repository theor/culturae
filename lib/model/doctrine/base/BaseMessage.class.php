<?php

/**
 * BaseMessage
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property string $text
 * @property integer $author_id
 * @property integer $addressee_id
 * @property enum $isnew
 * @property User $Author
 * @property User $Addressee
 * 
 * @method integer getId()           Returns the current record's "id" value
 * @method string  getText()         Returns the current record's "text" value
 * @method integer getAuthorId()     Returns the current record's "author_id" value
 * @method integer getAddresseeId()  Returns the current record's "addressee_id" value
 * @method enum    getIsnew()        Returns the current record's "isnew" value
 * @method User    getAuthor()       Returns the current record's "Author" value
 * @method User    getAddressee()    Returns the current record's "Addressee" value
 * @method Message setId()           Sets the current record's "id" value
 * @method Message setText()         Sets the current record's "text" value
 * @method Message setAuthorId()     Sets the current record's "author_id" value
 * @method Message setAddresseeId()  Sets the current record's "addressee_id" value
 * @method Message setIsnew()        Sets the current record's "isnew" value
 * @method Message setAuthor()       Sets the current record's "Author" value
 * @method Message setAddressee()    Sets the current record's "Addressee" value
 * 
 * @package    culturae
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7691 2011-02-04 15:43:29Z jwage $
 */
abstract class BaseMessage extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('message');
        $this->hasColumn('id', 'integer', null, array(
             'type' => 'integer',
             'primary' => true,
             'autoincrement' => true,
             ));
        $this->hasColumn('text', 'string', null, array(
             'type' => 'string',
             ));
        $this->hasColumn('author_id', 'integer', null, array(
             'type' => 'integer',
             ));
        $this->hasColumn('addressee_id', 'integer', null, array(
             'type' => 'integer',
             ));
        $this->hasColumn('isnew', 'enum', null, array(
             'type' => 'enum',
             'notnull' => true,
             'values' => 
             array(
              0 => 'False',
              1 => 'True',
             ),
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('User as Author', array(
             'local' => 'author_id',
             'foreign' => 'id',
             'onDelete' => 'CASCADE'));

        $this->hasOne('User as Addressee', array(
             'local' => 'addressee_id',
             'foreign' => 'id',
             'onDelete' => 'CASCADE'));

        $timestampable0 = new Doctrine_Template_Timestampable(array(
             'updated' => 
             array(
              'disabled' => true,
             ),
             ));
        $this->actAs($timestampable0);
    }
}