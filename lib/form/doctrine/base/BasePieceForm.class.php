<?php

/**
 * Piece form base class.
 *
 * @method Piece getObject() Returns the current form's model object
 *
 * @package    culturae
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BasePieceForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'          => new sfWidgetFormInputHidden(),
      'title'       => new sfWidgetFormTextarea(),
      'description' => new sfWidgetFormTextarea(),
      'image'       => new sfWidgetFormTextarea(),
      'kind'        => new sfWidgetFormChoice(array('choices' => array('Book' => 'Book', 'Movie' => 'Movie', 'Album' => 'Album'))),
      'genre'       => new sfWidgetFormChoice(array('choices' => array('SciFi' => 'SciFi', 'Horror' => 'Horror', 'Comedy' => 'Comedy'))),
      'author_id'   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Author'), 'add_empty' => true)),
    ));

    $this->setValidators(array(
      'id'          => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'title'       => new sfValidatorString(),
      'description' => new sfValidatorString(array('required' => false)),
      'image'       => new sfValidatorString(array('required' => false)),
      'kind'        => new sfValidatorChoice(array('choices' => array(0 => 'Book', 1 => 'Movie', 2 => 'Album'))),
      'genre'       => new sfValidatorChoice(array('choices' => array(0 => 'SciFi', 1 => 'Horror', 2 => 'Comedy'), 'required' => false)),
      'author_id'   => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Author'), 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('piece[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Piece';
  }

}
