<?php

/**
 * User form base class.
 *
 * @method User getObject() Returns the current form's model object
 *
 * @package    culturae
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseUserForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'mail'             => new sfWidgetFormTextarea(),
      'pass'             => new sfWidgetFormTextarea(),
      'firstname'        => new sfWidgetFormTextarea(),
      'lastname'         => new sfWidgetFormTextarea(),
      'avatar'           => new sfWidgetFormTextarea(),
      'ssoid'            => new sfWidgetFormTextarea(),
      'prescribers_list' => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'User')),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'mail'             => new sfValidatorString(),
      'pass'             => new sfValidatorString(),
      'firstname'        => new sfValidatorString(),
      'lastname'         => new sfValidatorString(),
      'avatar'           => new sfValidatorString(array('required' => false)),
      'ssoid'            => new sfValidatorString(array('required' => false)),
      'prescribers_list' => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'User', 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('user[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'User';
  }

  public function updateDefaultsFromObject()
  {
    parent::updateDefaultsFromObject();

    if (isset($this->widgetSchema['prescribers_list']))
    {
      $this->setDefault('prescribers_list', $this->object->Prescribers->getPrimaryKeys());
    }

  }

  protected function doSave($con = null)
  {
    $this->savePrescribersList($con);

    parent::doSave($con);
  }

  public function savePrescribersList($con = null)
  {
    if (!$this->isValid())
    {
      throw $this->getErrorSchema();
    }

    if (!isset($this->widgetSchema['prescribers_list']))
    {
      // somebody has unset this widget
      return;
    }

    if (null === $con)
    {
      $con = $this->getConnection();
    }

    $existing = $this->object->Prescribers->getPrimaryKeys();
    $values = $this->getValue('prescribers_list');
    if (!is_array($values))
    {
      $values = array();
    }

    $unlink = array_diff($existing, $values);
    if (count($unlink))
    {
      $this->object->unlink('Prescribers', array_values($unlink));
    }

    $link = array_diff($values, $existing);
    if (count($link))
    {
      $this->object->link('Prescribers', array_values($link));
    }
  }

}
