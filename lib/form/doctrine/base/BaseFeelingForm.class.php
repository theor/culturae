<?php

/**
 * Feeling form base class.
 *
 * @method Feeling getObject() Returns the current form's model object
 *
 * @package    culturae
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseFeelingForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'         => new sfWidgetFormInputHidden(),
      'value'      => new sfWidgetFormChoice(array('choices' => array('LikeValue' => 'LikeValue', 'DislikeValue' => 'DislikeValue'))),
      'comment_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Comment'), 'add_empty' => true)),
      'review_id'  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Review'), 'add_empty' => true)),
      'piece_id'   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Piece'), 'add_empty' => true)),
      'user_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('User'), 'add_empty' => true)),
      'created_at' => new sfWidgetFormDateTime(),
      'updated_at' => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'         => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'value'      => new sfValidatorChoice(array('choices' => array(0 => 'LikeValue', 1 => 'DislikeValue'))),
      'comment_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Comment'), 'required' => false)),
      'review_id'  => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Review'), 'required' => false)),
      'piece_id'   => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Piece'), 'required' => false)),
      'user_id'    => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('User'), 'required' => false)),
      'created_at' => new sfValidatorDateTime(),
      'updated_at' => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('feeling[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Feeling';
  }

}
