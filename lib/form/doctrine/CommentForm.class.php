<?php

/**
 * Comment form.
 *
 * @package    culturae
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class CommentForm extends BaseCommentForm
{
	public function configure()
	{
		$this->setWidgets(array(
	      'id'        => new sfWidgetFormInputHidden(),
	      'text'      => new sfWidgetFormTextarea(),
	      'user_id'   => new sfWidgetFormInputHidden(),
	      'review_id' => new sfWidgetFormInputHidden(),
		));

		$this->setValidators(array(
	      'id'        => new sfValidatorPass(),
	      'text'      => new sfValidatorString(array('required' => true)),
	      'user_id'   => new sfValidatorPass(),
	      'review_id' => new sfValidatorPass(),
		));

		$this->widgetSchema->setNameFormat('comment[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);



		$this->setDefault('review_id', $this->getOption('review'));
		$this->setDefault('user_id', Auth::getUserLogged()->getId());

		parent::configure();
	}
}
