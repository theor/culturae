<?php

/**
 * Review form.
 *
 * @package    culturae
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ReviewForm extends BaseReviewForm
{
	public function configure()
	{
		$this->setWidgets(array(
	      'id'          => new sfWidgetFormInputHidden(),
	      'grade'       => new sfWidgetFormChoice(array("choices" => array("0", "1", "2", "3", "4", "5"))),
	      'description' => new sfWidgetFormTextarea(),
	      'user_id'     => new sfWidgetFormInputHidden(),
	      'piece_id'    => new sfWidgetFormInputHidden()
		));

		$this->setValidators(array(
	      'id'          => new sfValidatorPass(),
	      'grade'       => new sfValidatorPass(),
	      'description' => new sfValidatorString(array('required' => true)),
	      'user_id'     => new sfValidatorPass(),
	      'piece_id'    => new sfValidatorPass(),
		));

		$this->widgetSchema->setNameFormat('review[%s]');

		$this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

			
		$this->setDefault('piece_id', $this->getOption('piece_id'));
		$this->setDefault('user_id', Auth::getUserLogged()->getId());

		parent::configure();
	}
}
