<?php

class SettingsForm extends BaseForm
{
  public function configure()
  {
    $this->setWidgets(array(
      	'firstname'   		=> new sfWidgetFormInput(),
      	'lastname'    		=> new sfWidgetFormInput(),
      	'mail'				=> new sfWidgetFormInput(),
    	'password'			=> new sfWidgetFormInputPassword(),
    	'confirmPassword'	=> new sfWidgetFormInputPassword(),
    	'avatar'			=> new sfWidgetFormInputFile()
    
    ));
    $this->widgetSchema->setNameFormat('settings[%s]');    
    
    $this->setValidators(array(
    	'firstname'			=> new sfValidatorString(array('required' => true)),
    	'lastname'			=> new sfValidatorString(array('required' => true)),
		'mail'    			=> new sfValidatorEmail(),
		'password'   		=> new sfValidatorString(array('required' => false)),
    	'confirmPassword'   => new sfValidatorString(array('required' => false)),
    	'avatar'			=> new sfValidatorFile(array('required' => false))
    ));
  }
}

?>