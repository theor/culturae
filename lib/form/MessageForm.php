<?php

class MessageForm extends BaseForm
{
  public function configure()
  {
    $this->setWidgets(array(
      	'addressee' => new sfWidgetFormInputHidden(),
      	'text'    	=> new sfWidgetFormTextarea()
    
    ));
    $this->widgetSchema->setNameFormat('message[%s]');    
    
    $this->setValidators(array(
    	'addressee'	=> new sfValidatorString(array('required' => true)),
    	'text'		=> new sfValidatorString(array('required' => true))
    ));
  }
}

?>