<?php

class LoginForm extends BaseForm
{
  public function configure()
  {
    $this->setWidgets(array(
      'mail'    => new sfWidgetFormInput(),
      'password'   => new sfWidgetFormInputPassword()
    ));
    $this->widgetSchema->setNameFormat('login[%s]');    
    
    $this->setValidators(array(
      'mail'    => new sfValidatorEmail(),
      'password'   => new sfValidatorString(array('required' => true))
    ));
    
  }
}

?>