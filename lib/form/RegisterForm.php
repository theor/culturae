<?php

class RegisterForm extends BaseForm
{
  public function configure()
  {
    $this->setWidgets(array(
      	'firstname'   		=> new sfWidgetFormInput(),
      	'lastname'    		=> new sfWidgetFormInput(),
      	'mail'				=> new sfWidgetFormInput(),
    	'password'			=> new sfWidgetFormInputPassword(),
    	'confirmPassword'	=> new sfWidgetFormInputPassword(),
    	'sso_id'			=> new sfWidgetFormInputHidden(),
    	'avatar'			=> new sfWidgetFormInputHidden()
    
    ));
    $this->widgetSchema->setNameFormat('register[%s]');    
    
    $this->setValidators(array(
    	'firstname'			=> new sfValidatorString(array('required' => true)),
    	'lastname'			=> new sfValidatorString(array('required' => true)),
		'mail'    			=> new sfValidatorEmail(),
		'password'   		=> new sfValidatorString(array('required' => true)),
    	'confirmPassword'   => new sfValidatorString(array('required' => true)),
    	'sso_id'   			=> new sfValidatorString(array('required' => false)),
    	'avatar'   			=> new sfValidatorString(array('required' => false)),
    ));
  }
}

?>