all:
	@echo "Modules:"
	@echo "  -doctrine : php symfony doctrine:generate-module --with-show --non-verbose-templates frontend user User"
	@echo "  -symfony :  php symfony generate:module frontend <module>"

.PHONY: data data-music data-movies

data: data-music data-movies

data-movies:
	./tools/make-data.rb movies | tee data/fixtures/movies.yml
data-music:
	./tools/make-data.rb music | tee data/fixtures/music.yml
db:
	php symfony doctrine:build --all --and-load --no-confirmation

db-clean:
	php symfony doctrine:clean

db-reset: db-clean db

clear-cache:
	php symfony cc

update:
	git stash && git pull --rebase && git stash pop

cc:
	php symfony cc
	git checkout cache/.gitignore